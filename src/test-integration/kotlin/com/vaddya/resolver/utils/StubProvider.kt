package com.vaddya.resolver.utils

import reactor.core.publisher.Mono

import java.nio.file.Files
import java.nio.file.Paths

class StubProvider {
    fun getDocumentsStub(): Mono<ByteArray> = get("documents/documents")

    fun getPersonsStub(): Mono<ByteArray> = get("persons/persons")

    fun getPersonStub(id: String): Mono<ByteArray> = get("persons/$id")

    fun getDocumentStub(id: String): Mono<ByteArray> = get("documents/$id")

    fun get(path: String): Mono<ByteArray> {
        return Mono.fromCallable {
            Files.readAllBytes(Paths.get("$STUBS_BASE_PATH/$path.json"))
        }
    }

    fun getResolved(path: String): String {
        return get("resolved/$path")
                .blockOptional()
                .map { String(it) }
                .orElseThrow()
    }

    companion object {
        private const val STUBS_BASE_PATH = "src/test-integration/resources/stubs"
    }
}
