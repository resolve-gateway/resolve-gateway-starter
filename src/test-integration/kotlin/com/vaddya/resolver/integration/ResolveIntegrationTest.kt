package com.vaddya.resolver.integration

import com.vaddya.resolver.utils.StubProvider
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.cloud.gateway.filter.GlobalFilter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ContextConfiguration(classes = [Config::class])
class ResolveIntegrationTest {
    @Autowired
    private lateinit var rest: WebTestClient

    @Autowired
    private lateinit var stubProvider: StubProvider

    @LocalServerPort
    private val port = 0

    @Test
    fun `Should resolve persons in documents using stubs from external service`() {
        rest.get().uri("http://localhost:$port/documents")
                .exchange()
                .expectStatus().isOk
                .expectBody()
                .consumeWith { println(it) }
                .json(stubProvider.getResolved("documents/documents"))
    }

    @Test
    fun `Should resolve person in a single document using stubs from external service`() {
        rest.get().uri("http://localhost:$port/documents/1")
                .header("Gateway-Resolvers", "documentsPerson")
                .header("Gateway-Post-Mappers", "fullName,summary")
                .exchange()
                .expectStatus().isOk
                .expectBody()
                .consumeWith { println(it) }
                .json(stubProvider.getResolved("documents/1"))
    }
}

@TestConfiguration
@EnableAutoConfiguration
@SpringBootConfiguration
open class Config {
    @Bean
    open fun webTestClient(): WebTestClient = WebTestClient.bindToServer().build()

    @Bean
    @Primary
    open fun webClient(personsExchangeFilterFunction: ExchangeFilterFunction): WebClient =
            WebClient.builder()
                    .filter(personsExchangeFilterFunction)
                    .build()

    @Bean
    open fun stubProvider(): StubProvider = StubProvider()

    @Bean
    open fun personsExchangeFilterFunction(stubProvider: StubProvider): ExchangeFilterFunction {
        return ExchangeFilterFunction { request, next ->
            if (request.url().port != 8082) {
                return@ExchangeFilterFunction next.exchange(request)
            }
            val path = request.url().path
            val data = if (path.contains(",")) {
                stubProvider.getPersonsStub()
            } else {
                val personId = path.substring(path.lastIndexOf('/') + 1)
                stubProvider.getPersonStub(personId)
            }
            data.map { bytes ->
                ClientResponse
                        .create(HttpStatus.OK)
                        .body(String(bytes))
                        .header("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .build()
            }
        }
    }

    @Bean
    open fun documentsGlobalFilter(stubProvider: StubProvider): GlobalFilter {
        return GlobalFilter { exchange, chain ->
            chain.filter(exchange).onErrorResume(Exception::class.java) {
                val response = exchange.response
                response.statusCode = HttpStatus.OK
                response.headers.contentType = MediaType.APPLICATION_JSON_UTF8

                val path = exchange.request.path.value()
                val data = if (path == "/documents") {
                    stubProvider.getDocumentsStub()
                } else {
                    val id = path.substring(path.lastIndexOf('/') + 1)
                    stubProvider.getDocumentStub(id)
                }
                data.map { bytes -> response.bufferFactory().wrap(bytes) }
                        .flatMap { buffer -> response.writeWith(Flux.just(buffer)) }
            }
        }
    }
}
