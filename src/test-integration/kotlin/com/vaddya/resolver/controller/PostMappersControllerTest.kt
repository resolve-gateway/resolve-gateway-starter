package com.vaddya.resolver.controller

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.autoconfigure.web.server.LocalManagementPort
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = [
    "management.port=0",
    "management.endpoints.web.exposure.include=resolve"
])
@ContextConfiguration(classes = [Config::class])
@AutoConfigureWebTestClient
class PostMappersControllerTest : AbstractControllerTest() {
    @Autowired
    override lateinit var rest: WebTestClient

    @LocalManagementPort
    private val port = 0

    override val path: String
        get() = "http://localhost:$port/actuator/resolve/postmappers"

    @Test
    fun `Should retrieve all post mappers`() {
        retrieve().isOk
                .expectBody()
                .consumeWith(System.out::println)
                .jsonPath("$").isArray
                .jsonPath("$[*].id").value<List<String>> {
                    assertTrue(it.contains("summary") && it.contains("fullName"))
                }
    }

    @Test
    fun `Should create and delete post mapper`() {
        val id = "testId"
        create(stub(id)).isOk

        retrieve(id).isOk
                .expectBody()
                .consumeWith { println(it) }
                .jsonPath("$.result").isEqualTo("field")

        delete(id).isOk

        retrieve(id).isNotFound
    }

    @Test
    fun `Should create and update post mapper field`() {
        val id = "testId"
        create(stub(id, result = "someField"))

        retrieve(id).isOk
                .expectBody()
                .jsonPath("$.result").isEqualTo("someField")

        update(id, stub(id, result = "anotherField")).isOk

        retrieve(id).isOk
                .expectBody()
                .jsonPath("$.result").isEqualTo("anotherField")
    }

    @Test
    fun `Should update post mapper id`() {
        val oldId = "oldId"
        val newId = "newId"
        create(stub(oldId)).isOk

        retrieve(oldId).isOk
        retrieve(newId).isNotFound

        update(oldId, stub(newId)).isOk

        retrieve(oldId).isNotFound
        retrieve(newId).isOk
    }

    private fun stub(
            id: String,
            expression: String = "2 + 2",
            result: String = "field"): String =
            """
                {
                    "id": "$id",
                    "expression": "$expression",
                    "result": "$result"
                }
            """
}
