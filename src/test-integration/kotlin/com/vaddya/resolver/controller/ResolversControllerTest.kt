package com.vaddya.resolver.controller

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.autoconfigure.web.server.LocalManagementPort
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = [
    "management.port=0",
    "management.endpoints.web.exposure.include=resolve"
])
@ContextConfiguration(classes = [Config::class])
@AutoConfigureWebTestClient
class ResolversControllerTest : AbstractControllerTest() {
    @Autowired
    override lateinit var rest: WebTestClient

    @LocalManagementPort
    private val port = 0

    override val path: String
        get() = "http://localhost:$port/actuator/resolve/resolvers"

    @Test
    fun `Should retrieve all resolvers`() {
        retrieve().isOk
                .expectBody()
                .consumeWith { println(it) }
                .jsonPath("$").isArray
                .jsonPath("$[*].id").value<List<String>> {
                    assertTrue(it.contains("documentsPerson") && it.contains("selfPerson"))
                }
    }

    @Test
    fun `Should create and delete resolver`() {
        val id = "testId"
        create(stub(id)).isOk

        retrieve(id).isOk
                .expectBody()
                .consumeWith { println(it) }
                .jsonPath("$.source.field").isEqualTo("sourceField")
                .jsonPath("$.result").isEqualTo("resultField")

        delete(id).isOk

        retrieve(id).isNotFound
    }

    @Test
    fun `Should update resolver field`() {
        val id = "testId"
        create(stub(id, source = "someSourceField"))

        retrieve(id).isOk
                .expectBody()
                .jsonPath("$.source.field").isEqualTo("someSourceField")

        update(id, stub(id, source = "anotherSourceField")).isOk

        retrieve(id).isOk
                .expectBody()
                .jsonPath("$.source.field").isEqualTo("anotherSourceField")
    }

    @Test
    fun `Should update resolver id`() {
        val oldId = "oldId"
        val newId = "newId"
        create(stub(oldId)).isOk

        retrieve(oldId).isOk
        retrieve(newId).isNotFound

        update(oldId, stub(newId)).isOk

        retrieve(oldId).isNotFound
        retrieve(newId).isOk
    }

    private fun stub(
            id: String,
            source: String = "sourceField",
            uri: String = "lb://test",
            result: String = "resultField"): String =
            """
                {
                    "id": "$id",
                    "source": {
                        "field": "$source",
                        "uri": "$uri"
                    },
                    "result": "$result"
                }
            """
}
