package com.vaddya.resolver.controller

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.autoconfigure.web.server.LocalManagementPort
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = [
    "management.port=0",
    "management.endpoints.web.exposure.include=resolve"
])
@ContextConfiguration(classes = [Config::class])
@AutoConfigureWebTestClient
class ResolveRoutesControllerTest : AbstractControllerTest() {
    @Autowired
    override lateinit var rest: WebTestClient

    @LocalManagementPort
    private val port = 0

    override val path: String
        get() = "http://localhost:$port/actuator/resolve/routes"

    @Test
    fun `Should retrieve all resolve routes`() {
        retrieve().isOk
                .expectBody()
                .jsonPath("$").isArray
                .jsonPath("$[*].id").value<List<String>> {
                    assertTrue(it.contains("documents"))
                }
    }

    @Test
    fun `Should create and delete resolve route`() {
        val id = "persons"
        create(stub(id)).isOk

        retrieve(id).isOk
                .expectBody()
                .jsonPath("$.collection").isEqualTo("colName")

        delete(id).isOk

        retrieve(id).isOk
                .expectBody()
                .jsonPath("$.collection").isEqualTo("")
    }

    @Test
    fun `Should update resolve route field`() {
        val id = "persons"
        create(stub(id, collection = "someCol"))

        retrieve(id).isOk
                .expectBody()
                .consumeWith { println(it) }
                .jsonPath("$.collection").isEqualTo("someCol")

        update(id, stub(id, collection = "anotherCol")).isOk

        retrieve(id).isOk
                .expectBody()
                .jsonPath("$.collection").isEqualTo("anotherCol")
    }

    private fun stub(
            id: String,
            collection: String = "colName"): String =
            """
                {
                    "id": "$id",
                    "useCollection": false,
                    "collection": "$collection",
                    "resolvers": [],
                    "postMapper": []
                }
            """
}

