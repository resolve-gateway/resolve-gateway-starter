package com.vaddya.resolver.controller

import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.StatusAssertions
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters

abstract class AbstractControllerTest {
    abstract var rest: WebTestClient

    abstract val path: String

    protected fun create(body: String): StatusAssertions {
        return rest
                .post()
                .uri(path)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(body))
                .exchange()
                .expectStatus()
    }

    protected fun retrieve(id: String = ""): StatusAssertions {
        return rest
                .get()
                .uri("$path/$id")
                .exchange()
                .expectStatus()
    }

    protected fun update(id: String, body: String): StatusAssertions {
        return rest
                .post()
                .uri("$path/$id")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(body))
                .exchange()
                .expectStatus()
    }

    protected fun delete(id: String): StatusAssertions {
        return rest
                .delete()
                .uri("$path/$id")
                .exchange()
                .expectStatus()
    }
}

@TestConfiguration
@EnableAutoConfiguration
@SpringBootConfiguration
open class Config

