package com.vaddya.resolver.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.TextNode
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.vaddya.resolver.domain.Resolver
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import reactor.test.StepVerifier

class HrefFieldResolverTest {
    private val hrefFieldResolver: HrefFieldResolver
    private val json: ObjectMapper = ObjectMapper()

    init {
        val cacheService: CacheService = mock {
            val node = json.createObjectNode().put("name", "testName")
            on { exchange(any(), eq("lb://test/42")) }.thenReturn(Mono.just(node))
        }
        this.hrefFieldResolver = HrefFieldResolver(cacheService)
    }

    @Test
    fun `Should resolve field by href`() {
        val resolver = Resolver.builder().build()
        val idNode = TextNode("lb://test/42")
        val result = hrefFieldResolver.resolve(resolver, idNode)
        StepVerifier.create(result)
                .expectNextMatches { it["name"].textValue() == "testName" }
                .verifyComplete()
    }
}
