package com.vaddya.resolver.service

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.vaddya.resolver.util.JsonPath
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class JsonWalkerTest {
    private val walker = JsonWalker()
    private val json = ObjectMapper()

    @Test
    fun `Should return root if path is empty`() {
        val root = nodeStub()
        val result = walker.obj(root, JsonPath.empty(), false)
        assertSame(root, result)
    }

    @Test
    fun `Should walk 1 level`() {
        val root = nodeStub()
        val result = walker.obj(root, JsonPath.from("key1"), false)

        assertSame(root["key1"], result)
        assertTrue(result.has("key2"))
        assertThat(result.get("key2")).isInstanceOf(ObjectNode::class.java)
    }

    @Test
    fun `Should walk 2 levels`() {
        val root = nodeStub()
        val result = walker.obj(root, JsonPath.from("key1.key2"), false)

        assertThat(result).isSameAs(root["key1"]["key2"])
        assertThat(result["key3"].textValue()).isEqualTo("value")
        assertThat(root["key1"]["key2"]).isNotNull
        assertThat(root["key1"]["key2"]["key3"]).isNotNull
        assertThat(root["key1"]["key2"]["key3"].textValue()).isEqualTo("value")
    }


    @Test
    fun `Should create node on walking`() {
        val root = nodeStub()
        val result = walker.obj(root, JsonPath.from("key1.key4.key5"), true)

        assertThat(root["key1"]).isNotNull
        assertThat(root["key1"]).isInstanceOf(ObjectNode::class.java)

        val createdNode = root["key1"]["key4"]

        assertThat(createdNode).isNotNull
        assertThat(createdNode).isInstanceOf(ObjectNode::class.java)
        assertThat(createdNode["key5"]).isNotNull
        assertThat(createdNode["key5"]).allMatch { it.size() == 0 }
        assertThat(result).isSameAs(root["key1"]["key4"]["key5"])
    }

    private fun nodeStub(): JsonNode {
        val data = """
            {
                "key1": {
                    "key2": {
                        "key3": "value"
                    }
                }
            }
        """
        return json.readTree(data)
    }
}
