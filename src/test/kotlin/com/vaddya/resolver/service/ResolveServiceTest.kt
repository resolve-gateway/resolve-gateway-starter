package com.vaddya.resolver.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.vaddya.resolver.domain.Resolver
import com.vaddya.resolver.domain.Resolver.Source
import com.vaddya.resolver.util.JsonPath
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.net.URI
import java.util.UUID.randomUUID

class ResolveServiceTest {
    private val resolveService: ResolveService
    private val json: ObjectMapper = ObjectMapper()

    init {
        val cacheService: CacheService = mock {
            val person = json.createObjectNode().put("name", "testName")
            on { exchange(any(), eq("lb://test/1")) }.thenReturn(Mono.just(person))
        }
        this.resolveService = ResolveService(cacheService, JsonWalker())
    }

    @Test
    fun `Should resolve using href resolver`() {
        val data = """
            {
                "name": "testName",
                "_links": {
                    "friend": {
                        "href": "lb://test/1"
                    }
                }
            }
        """
        val node = json.readTree(data)
        val resolver = Resolver.builder()
                .source(Source.builder()
                        .field(JsonPath.from("_links.friend.href"))
                        .build())
                .result(JsonPath.from("_embedded.friend"))
                .build()
        val result = resolveService.resolve(randomUUID(), node as ObjectNode, listOf(resolver))
        StepVerifier.create(result)
                .expectNextMatches { it["_embedded"]["friend"]["name"].asText() == "testName" }
                .verifyComplete()
    }

    @Test
    fun `Should resolve using uri resolver`() {
        val data = """
            {
                "id": 1,
                "name": "testName"
            }
        """
        val node = json.readTree(data)
        val resolver = Resolver.builder()
                .source(Source.builder()
                        .field(JsonPath.from("id"))
                        .uri(URI.create("lb://test"))
                        .build())
                .result(JsonPath.from("_embedded.friend"))
                .build()
        val result = resolveService.resolve(randomUUID(), node as ObjectNode, listOf(resolver))
        StepVerifier.create(result)
                .expectNextMatches { it["_embedded"]["friend"]["name"].asText() == "testName" }
                .verifyComplete()
    }

    @Test
    fun `Should resolve using multiple resolvers`() {
        val data = """
            {
                "id": 1,
                "name": "testName",
                "_links": {
                    "friend": {
                        "href": "lb://test/1"
                    }
                }
            }
        """
        val node = json.readTree(data)
        val hrefResolver = Resolver.builder()
                .source(Source.builder()
                        .field(JsonPath.from("_links.friend.href"))
                        .build())
                .result(JsonPath.from("_embedded.friend"))
                .build()
        val uriResolver = Resolver.builder()
                .source(Source.builder()
                        .field(JsonPath.from("id"))
                        .uri(URI.create("lb://test"))
                        .build())
                .result(JsonPath.from("_embedded.self"))
                .build()
        val result = resolveService.resolve(randomUUID(), node as ObjectNode, listOf(hrefResolver, uriResolver))
        StepVerifier.create(result)
                .expectNextMatches {
                    it["_embedded"]["friend"]["name"].asText() == "testName" &&
                            it["_embedded"]["self"]["name"].asText() == "testName"
                }
                .verifyComplete()
    }
}
