package com.vaddya.resolver.util

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class JsonPathTest {
    @Test
    fun `Should create path from valid source`() {
        val path = JsonPath.from("_embedded.savings")

        assertFalse(path.isEmpty)
        assertEquals("_embedded.savings", path.toString())
        assertEquals("_embedded", path.get(0))
        assertEquals("savings", path.get(1))
        assertEquals("savings", path.last())
    }

    @Test
    fun `Should create empty path`() {
        val path = JsonPath.from("")
        val pathFromNull = JsonPath.from(null)

        assertTrue(path.isEmpty)
        assertTrue(pathFromNull.isEmpty)
        assertThrows(IndexOutOfBoundsException::class.java) { path.get(0) }
        assertEquals(JsonPath.empty(), path)
    }

    @Test
    fun `Should throw an exception for invalid source`() {
        assertThrows(IllegalArgumentException::class.java) { JsonPath.from("_embedded savings") }
    }

    @Test
    fun `Should return new path without first or last`() {
        val path = JsonPath.from("_embedded.savings")
        val withoutFirst = path.withoutFirst()
        val withoutLast = path.withoutLast()


        assertNotSame(path, withoutFirst)
        assertNotSame(path, withoutLast)
        assertNotSame(withoutFirst, withoutLast)

        assertEquals("savings", withoutFirst.toString())
        assertEquals("_embedded", withoutLast.toString())
    }

    @Test
    fun `Should return new extended path`() {
        val path = JsonPath.from("_embedded")
        val added = path.add("savings")

        assertNotSame(path, added)
        assertEquals("_embedded.savings", added.toString())
        assertEquals("_embedded", added.get(0))
        assertEquals("savings", added.last())
    }

    @Test
    fun `Should return first part of the path`() {
        val path = JsonPath.from("_embedded.savings")
        val first = path.first()

        assertEquals("_embedded", first)
    }
}
