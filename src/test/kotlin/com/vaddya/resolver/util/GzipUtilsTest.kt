package com.vaddya.resolver.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class GzipUtilsTest {
    @Test
    fun `Should correctly encode and decode gzip`() {
        val original = "1234"
        val bytes = GzipUtils.encodeGzip(original.toByteArray())
        val decoded = String(GzipUtils.decodeGzip(bytes))

        assertEquals(original, decoded)
    }
}
