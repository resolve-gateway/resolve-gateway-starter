package com.vaddya.resolver.controller;

import com.vaddya.resolver.converter.ResolveRoutesConverter;
import com.vaddya.resolver.domain.ResolveRoute;
import com.vaddya.resolver.domain.ResolveRouteProps;
import com.vaddya.resolver.repository.ResolveRoutesRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller that handles CRUD operations on Resolve Routes
 */
@RequiredArgsConstructor
public class ResolveRoutesController implements EntityController<ResolveRouteProps> {
    private final ResolveRoutesRepository repository;
    private final ResolveRoutesConverter converter;

    @Override
    public Mono<ResolveRouteProps> create(@RequestBody final Mono<ResolveRouteProps> props) {
        return converter.fromProps(props)
                .flatMap(repository::put)
                .flatMap(converter::toProps);
    }

    @Override
    public Flux<ResolveRouteProps> retrieve() {
        return repository.getAll()
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<ResolveRouteProps> retrieveById(@PathVariable final String id) {
        return repository.get(id)
                .flatMap(converter::toProps)
                .switchIfEmpty(Mono.fromCallable(() -> ResolveRouteProps.empty(id)));
    }

    @Override
    public Mono<ResolveRouteProps> update(
            @PathVariable final String id,
            @RequestBody final Mono<ResolveRouteProps> props) {
        return converter.fromProps(props)
                .flatMap(route -> removeIfEntityIdChanged(id, route))
                .flatMap(repository::put)
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<Void> delete(@PathVariable final String id) {
        return repository.remove(id);
    }

    private Mono<ResolveRoute> removeIfEntityIdChanged(
            @NotNull final String id,
            @NotNull final ResolveRoute route) {
        return Mono.just(id.equals(route.getRoute().getId())
                                 ? Mono.empty()
                                 : repository.remove(id))
                .thenReturn(route);
    }
}
