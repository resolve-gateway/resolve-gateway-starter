package com.vaddya.resolver.controller;

import com.vaddya.resolver.converter.PostMappersConverter;
import com.vaddya.resolver.domain.PostMapper;
import com.vaddya.resolver.domain.PostMapperProps;
import com.vaddya.resolver.repository.PostMappersRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller that handles CRUD operations on Post Mappers
 */
@RequiredArgsConstructor
public class PostMappersController implements EntityController<PostMapperProps> {
    private final PostMappersRepository repository;
    private final PostMappersConverter converter;

    @Override
    public Mono<PostMapperProps> create(Mono<PostMapperProps> props) {
        return converter.fromProps(props)
                .flatMap(repository::put)
                .flatMap(converter::toProps);
    }

    @Override
    public Flux<PostMapperProps> retrieve() {
        return repository.getAll()
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<PostMapperProps> retrieveById(String id) {
        return repository.get(id)
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<PostMapperProps> update(
            @PathVariable final String id,
            @RequestBody final Mono<PostMapperProps> props) {
        return converter.fromProps(props)
                .flatMap(postMapper -> removeIfEntityIdChanged(id, postMapper))
                .flatMap(repository::put)
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<Void> delete(@PathVariable final String id) {
        return repository.remove(id);
    }

    private Mono<PostMapper> removeIfEntityIdChanged(
            @NotNull final String id,
            @NotNull final PostMapper postMapper) {
        return Mono.just(id.equals(postMapper.getId())
                                 ? Mono.empty()
                                 : repository.remove(id))
                .thenReturn(postMapper);
    }
}
