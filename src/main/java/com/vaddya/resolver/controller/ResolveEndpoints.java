package com.vaddya.resolver.controller;

import com.vaddya.resolver.domain.PostMapperProps;
import com.vaddya.resolver.domain.ResolveRouteProps;
import com.vaddya.resolver.domain.ResolverProps;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.NoSuchElementException;

/**
 * REST API Endpoints exposed to support CRUD operations on Resolvers,
 * Post Mappers and Resolve Routes using Spring Boot Actuator library
 */
@RestControllerEndpoint(id = "resolve")
@RequiredArgsConstructor
@Log4j2
public class ResolveEndpoints {
    private final ResolversController resolverController;
    private final PostMappersController postMapperController;
    private final ResolveRoutesController resolveRouteController;

    /**
     * Exception handlers
     */
    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such element")
    @ExceptionHandler(NoSuchElementException.class)
    public void noSuchElementException(NoSuchElementException e) {
        log.error(e);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Illegal arguments")
    @ExceptionHandler(IllegalArgumentException.class)
    public void illegalArgumentException(IllegalArgumentException e) {
        log.error(e);
    }

    /**
     * Index web page
     */
    @GetMapping
    public Resource index() {
        return new ClassPathResource("/public/index.html");
    }

    /**
     * Resolvers
     */
    @PostMapping("/resolvers")
    public Mono<ResolverProps> createResolver(@RequestBody final Mono<ResolverProps> props) {
        return resolverController.create(props);
    }

    @GetMapping("/resolvers")
    public Flux<ResolverProps> retrieveResolvers() {
        return resolverController.retrieve();
    }

    @GetMapping("/resolvers/{id}")
    public Mono<ResolverProps> retrieveResolverById(@PathVariable final String id) {
        return resolverController.retrieveById(id);
    }

    @PostMapping("/resolvers/{id}")
    public Mono<ResolverProps> updateResolver(
            @PathVariable final String id,
            @RequestBody final Mono<ResolverProps> props) {
        return resolverController.update(id, props);
    }

    @DeleteMapping("/resolvers/{id}")
    public Mono<Void> deleteResolver(@PathVariable final String id) {
        return resolverController.delete(id);
    }

    /**
     * Post Mappers
     */
    @PostMapping("/postmappers")
    public Mono<PostMapperProps> createPostMapper(@RequestBody final Mono<PostMapperProps> props) {
        return postMapperController.create(props);
    }

    @GetMapping("/postmappers")
    public Flux<PostMapperProps> retrievePostMappers() {
        return postMapperController.retrieve();
    }

    @GetMapping("/postmappers/{id}")
    public Mono<PostMapperProps> retrievePostMapperById(@PathVariable final String id) {
        return postMapperController.retrieveById(id);
    }

    @PostMapping("/postmappers/{id}")
    public Mono<PostMapperProps> updatePostMapper(
            @PathVariable final String id,
            @RequestBody final Mono<PostMapperProps> props) {
        return postMapperController.update(id, props);
    }

    @DeleteMapping("/postmappers/{id}")
    public Mono<Void> deletePostMapper(@PathVariable final String id) {
        return postMapperController.delete(id);
    }

    /**
     * Resolve Routes
     */
    @PostMapping("/routes")
    public Mono<ResolveRouteProps> createResolveRoute(@RequestBody final Mono<ResolveRouteProps> props) {
        return resolveRouteController.create(props);
    }

    @GetMapping("/routes")
    public Flux<ResolveRouteProps> retrieveResolveRoutes() {
        return resolveRouteController.retrieve();
    }

    @GetMapping("/routes/{id}")
    public Mono<ResolveRouteProps> retrieveResolveRouteById(@PathVariable final String id) {
        return resolveRouteController.retrieveById(id);
    }

    @PostMapping("/routes/{id}")
    public Mono<ResolveRouteProps> updateResolveRoute(
            @PathVariable final String id,
            @RequestBody final Mono<ResolveRouteProps> props) {
        return resolveRouteController.update(id, props);
    }

    @DeleteMapping("/routes/{id}")
    public Mono<Void> deleteResolveRoute(@PathVariable final String id) {
        return resolveRouteController.delete(id);
    }
}
