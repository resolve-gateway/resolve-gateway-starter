package com.vaddya.resolver.controller;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller that handles CRUD operations on entities of type P
 */
public interface EntityController<P> {
    /**
     * Create from entity
     */
    Mono<P> create(final Mono<P> props);

    /**
     * Retrieve all entities
     */
    Flux<P> retrieve();

    /**
     * Retrieve entry by ID
     */

    Mono<P> retrieveById(final String id);

    /**
     * Update entity by ID
     */
    Mono<P> update(
            final String id,
            final Mono<P> props);

    /**
     * Delete entity by ID
     */
    Mono<Void> delete(final String id);
}
