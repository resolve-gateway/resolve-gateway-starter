package com.vaddya.resolver.controller;

import com.vaddya.resolver.converter.ResolversConverter;
import com.vaddya.resolver.domain.Resolver;
import com.vaddya.resolver.domain.ResolverProps;
import com.vaddya.resolver.repository.ResolversRepository;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Controller that handles CRUD operations on Resolvers
 */
@RequiredArgsConstructor
public class ResolversController implements EntityController<ResolverProps> {
    private final ResolversRepository repository;
    private final ResolversConverter converter;

    @Override
    public Mono<ResolverProps> create(@RequestBody final Mono<ResolverProps> props) {
        return converter.fromProps(props)
                .flatMap(repository::put)
                .flatMap(converter::toProps);
    }

    @Override
    public Flux<ResolverProps> retrieve() {
        return repository.getAll()
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<ResolverProps> retrieveById(String id) {
        return repository.get(id)
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<ResolverProps> update(
            @PathVariable final String id,
            @RequestBody final Mono<ResolverProps> props) {
        return converter.fromProps(props)
                .flatMap(resolver -> removeIfEntityIdChanged(id, resolver))
                .flatMap(repository::put)
                .flatMap(converter::toProps);
    }

    @Override
    public Mono<Void> delete(@PathVariable final String id) {
        return repository.remove(id);
    }

    private Mono<Resolver> removeIfEntityIdChanged(
            @NotNull final String id,
            @NotNull final Resolver resolver) {
        return Mono.just(id.equals(resolver.getId())
                                 ? Mono.empty()
                                 : repository.remove(id))
                .thenReturn(resolver);
    }
}
