package com.vaddya.resolver.domain;

import com.vaddya.resolver.util.JsonPath;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResolveRouteProps {
    private String id;
    private Boolean useCollection = false;
    private String collection = "";
    private List<String> resolvers = new ArrayList<>();
    private List<String> postMappers = new ArrayList<>();

    public static ResolveRouteProps empty(String id) {
        return new ResolveRouteProps(
                id,
                false,
                JsonPath.empty().toString(),
                emptyList(),
                emptyList()
        );
    }
}
