package com.vaddya.resolver.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostMapperProps {
    private String id;
    private String expression;
    private String result;
}
