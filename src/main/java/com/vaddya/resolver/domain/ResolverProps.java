package com.vaddya.resolver.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResolverProps {
    private String id;
    private Source source;
    @Nullable
    private String result;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Source {
        private String field;
        @Nullable
        private String uri;
        @Nullable
        private Batch batch;

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Batch {
            private String uri;
            @Builder.Default
            private String collection = "";
            @Builder.Default
            private String query = "id";
            @Builder.Default
            private String delimiter = ",";
        }
    }
}
