package com.vaddya.resolver.domain;

import com.vaddya.resolver.util.JsonPath;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.jetbrains.annotations.Nullable;

import java.net.URI;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder
@AllArgsConstructor(access = PRIVATE)
public class Resolver {
    private String id;
    @Nullable
    private JsonPath result;
    private Source source;

    @Value
    @Builder
    public static class Source {
        private JsonPath field;
        @Nullable
        private URI uri;
        @Nullable
        private Batch batch;

        @Value
        @Builder
        public static class Batch {
            private URI uri;
            private JsonPath collection;
            private String query;
            private String delimiter;
        }
    }
}
