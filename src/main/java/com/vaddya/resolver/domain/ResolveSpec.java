package com.vaddya.resolver.domain;

import com.vaddya.resolver.util.JsonPath;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;

@Data
@Builder
@AllArgsConstructor(access = PRIVATE)
public class ResolveSpec {
    private UUID id;
    private Boolean useCollection;
    private JsonPath collection;
    private List<Resolver> resolvers;
    private List<PostMapper> postMappers;
}
