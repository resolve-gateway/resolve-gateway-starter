package com.vaddya.resolver.domain;

import com.vaddya.resolver.util.JsonPath;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.springframework.expression.Expression;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder
@AllArgsConstructor(access = PRIVATE)
public class PostMapper {
    private String id;
    private Expression expression;
    private JsonPath result;
}
