package com.vaddya.resolver.domain;

import com.vaddya.resolver.util.JsonPath;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.springframework.cloud.gateway.route.Route;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder
@AllArgsConstructor(access = PRIVATE)
public class ResolveRoute {
    private final Route route;
    private final Boolean useCollection;
    private final JsonPath collection;
    private final List<Resolver> resolvers;
    private final List<PostMapper> postMappers;
}
