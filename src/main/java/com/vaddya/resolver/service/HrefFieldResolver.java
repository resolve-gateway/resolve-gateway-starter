package com.vaddya.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaddya.resolver.domain.Resolver;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Deprecated
@RequiredArgsConstructor
public class HrefFieldResolver implements FieldResolver {
    private final CacheService cacheService;

    @Override
    @NotNull
    public Mono<JsonNode> resolve(
            @NotNull final Resolver resolver,
            @NotNull final JsonNode node) {
        String url = node.textValue();
        return cacheService.exchange(UUID.randomUUID(), url);
    }
}
