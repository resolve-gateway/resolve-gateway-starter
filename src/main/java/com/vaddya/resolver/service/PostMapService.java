package com.vaddya.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaddya.resolver.domain.PostMapper;
import com.vaddya.resolver.service.exception.InvalidJsonPathException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.expression.spel.SpelEvaluationException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

import static java.util.function.Function.identity;

/**
 * Service that handles post mapping operations on the given object
 */
@Log4j2
@RequiredArgsConstructor
public class PostMapService {
    private final JsonWalker walker;
    private final ObjectMapper json;

    public Mono<JsonNode> map(
            @NotNull final JsonNode object,
            @NotNull final Iterable<PostMapper> postMappers) {
        return Flux.fromIterable(postMappers)
                .reduce(Mono.just(object), this::map)
                .flatMap(identity())
                .switchIfEmpty(Mono.just(object));
    }

    public Mono<JsonNode> map(
            @NotNull final Mono<JsonNode> node,
            @NotNull final PostMapper postMapper) {
        final var path = postMapper.getResult().withoutLast();
        final var field = postMapper.getResult().last();
        return node.map(object -> {
            final var map = json.convertValue(object, Map.class);
            try {
                final var resultMap = walker.obj(object, path, true);
                final var result = postMapper.getExpression().getValue(map, String.class);
                resultMap.put(field, result);
            } catch (SpelEvaluationException | InvalidJsonPathException e) {
                log.error("Cannot map: {}", e.getMessage());
            }
            return log.traceExit("PostMapped resource", object);
        });
    }
}
