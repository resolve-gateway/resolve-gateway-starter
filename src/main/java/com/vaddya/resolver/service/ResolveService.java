package com.vaddya.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaddya.resolver.domain.Resolver;
import com.vaddya.resolver.service.exception.ResolveException;
import com.vaddya.resolver.util.JsonPath;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.function.Function.identity;

/**
 * Service that resolves data dependency on the given objects
 */
@Log4j2
@RequiredArgsConstructor
public class ResolveService {
    private static final JsonPath DEFAULT_PATH = JsonPath.from("_embedded");

    private final CacheService cacheService;
    private final JsonWalker walker;

    public Mono<ObjectNode> resolve(
            @NotNull final UUID id,
            @NotNull final ObjectNode object,
            @NotNull final Iterable<Resolver> resolvers) {
        return Flux.fromIterable(resolvers)
                .flatMap(resolver -> resolveOne(id, object, resolver))
                .then()
                .thenReturn(object);
    }

    public Mono<ArrayNode> resolve(
            @NotNull final UUID id,
            @NotNull final ArrayNode objects,
            @NotNull final Iterable<Resolver> resolvers) {
        return Flux.fromIterable(resolvers)
                .flatMap(resolver -> resolveMany(id, objects, resolver))
                .then()
                .thenReturn(objects);
    }

    private Mono<Void> resolveOne(
            @NotNull final UUID id,
            @NotNull final ObjectNode object,
            @NotNull final Resolver resolver) {
        return Mono.just(request(object, resolver))
                .map(request -> log.traceExit("Resolve request: {}", request))
                .flatMap(request -> cacheService
                        .exchange(id, request.id)
                        .map(request.callback))
                .then();
    }

    private Mono<Void> resolveMany(
            @NotNull final UUID id,
            @NotNull final ArrayNode objects,
            @NotNull final Resolver resolver) {
        if (resolver.getSource().getBatch() != null) {
            return Flux.fromIterable(objects)
                    .map(obj -> request((ObjectNode) obj, resolver))
                    .map(request -> log.traceExit("Resolve request: {}", request))
                    .collectList()
                    .flatMap(requests -> batchRequest(id, requests, resolver));
        } else {
            return Flux.fromIterable(objects)
                    .map(obj -> request((ObjectNode) obj, resolver))
                    .map(request -> log.traceExit("Resolve request: {}", request))
                    .groupBy(request -> request.id)
                    .flatMap(requests -> cacheService
                            .exchange(id, Objects.requireNonNull(requests.key()))
                            .flatMapMany(response -> requests
                                    .map(request -> request.callback.apply(response))))
                    .then();
        }
    }

    private Mono<Void> batchRequest(
            @NotNull final UUID id,
            @NotNull final List<ResolveRequest> requests,
            @NotNull final Resolver resolver) {
        final var batch = resolver.getSource().getBatch();
        if (batch == null) {
            log.error("Cannot make batch request for requests {} using resolver {}", requests, resolver);
            return Mono.empty();
        }
        final var ids = requests.stream()
                .map(request -> request.id)
                .distinct()
                .collect(Collectors.joining(batch.getDelimiter()));
        final var href = createBatchHref(batch, ids);
        log.trace("Making batch request to {}", href);
        return cacheService.exchange(id, href)
                .map(node -> walker.array(node, batch.getCollection()))
                .map(this::arrayToMapById)
                .map(batchMap -> {
                    log.trace("Batch map: {}", batchMap);
                    requests.forEach(request -> {
                        JsonNode resolved = batchMap.get(request.id);
                        request.callback.apply(resolved);
                    });
                    return batchMap;
                })
                .then();
    }

    private ResolveRequest request(
            @NotNull final ObjectNode object,
            @NotNull final Resolver resolver) {
        final JsonPath source = resolver.getSource().getField();
        if (source.isEmpty()) {
            log.error("Source field is empty in resolver: {}", resolver);
            throw new ResolveException("Source field is empty");
        }

        final JsonPath result = resolver.getResult();
        final JsonPath path;
        final String resultField;
        if (result == null) {
            path = DEFAULT_PATH;
            resultField = source.last();
        } else {
            path = result.withoutLast();
            resultField = result.last();
        }
        final var resultNode = walker.obj(object, path, true);

        log.trace("Resolving field {} = {} using resolver {}", result, source, resolver);

        final var id = extractId(object, resolver);
        return new ResolveRequest(id, node -> resultNode.set(resultField, node));
    }

    private String extractId(ObjectNode node, Resolver resolver) {
        final var source = resolver.getSource().getField();
        final var text = walker.node(node, source, false).asText();
        if (resolver.getSource().getUri() != null) {
            return resolver.getSource().getUri().toString() + "/" + text;
        } else {
            return text;
        }
    }

    private String createBatchHref(Resolver.Source.Batch batch, String ids) {
        return String.format("%s?%s=%s", batch.getUri().toString(), batch.getQuery(), ids);
    }

    private Map<String, JsonNode> arrayToMapById(ArrayNode array) {
        return StreamSupport.stream(array.spliterator(), false)
                .collect(Collectors.toMap(o -> o.get("id").asText(), identity()));
    }

    @AllArgsConstructor
    @ToString(of = "id")
    private static class ResolveRequest {
        String id; // entity id or href
        UnaryOperator<JsonNode> callback;
    }
}
