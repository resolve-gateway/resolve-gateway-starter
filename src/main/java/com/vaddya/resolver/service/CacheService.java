package com.vaddya.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Service that makes blocking http requests and caches results
 */
@Log4j2
public class CacheService {
    private final WebClient web;
    private final Map<UUID, LoadingCache<String, Mono<JsonNode>>> cache;

    public CacheService(WebClient web) {
        this.web = web;
        this.cache = new ConcurrentHashMap<>();
    }

    @NotNull
    public Mono<JsonNode> exchange(
            @NotNull final UUID id,
            @NotNull final String href) {
        log.trace("Cache request: {}", href);
        cache.computeIfAbsent(id, uuid -> Caffeine.newBuilder().build(this::asyncExchange));
        Mono<JsonNode> response = cache.get(id).get(href);
        if (response == null) {
            throw new IllegalStateException("Empty return value");
        }
        return response.map(JsonNode::deepCopy);
    }

    @NotNull
    private Mono<JsonNode> asyncExchange(@NotNull final String href) {
        log.trace("Cache makes async exchange: {}", href);
        return web.get()
                .uri(href)
                .exchange()
                .doOnError(ex -> log.error("Error on {}: {}", href, ex))
                .doOnSuccess(resp -> log.trace("Success on {}: {}", () -> href, resp::statusCode))
                .flatMap(it -> it.bodyToMono(JsonNode.class))
                .cache();
    }
}
