package com.vaddya.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaddya.resolver.domain.Resolver;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Deprecated
@Log4j2
@RequiredArgsConstructor
public class UriFieldResolver implements FieldResolver {
    private final CacheService cacheService;

    @Override
    public @NotNull Mono<JsonNode> resolve(
            @NotNull final Resolver resolver,
            @NotNull final JsonNode node) {
        final var uri = resolver.getSource().getUri();
        if (uri == null) {
            log.error("Empty URI in resolver: {}", resolver.getId());
            throw new IllegalArgumentException("Empty URI");
        }
        if (node.isContainerNode()) {
            log.error("Node passed to UriResolver is container: {}", node);
            throw new IllegalArgumentException("Non-textual node");
        }

        var href = uri.toString();
        if (!href.endsWith("/")) {
            href = href + "/";
        }
        href = href + node.asText();

        return cacheService.exchange(UUID.randomUUID(), href);
    }
}
