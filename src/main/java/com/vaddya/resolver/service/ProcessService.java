package com.vaddya.resolver.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaddya.resolver.domain.ResolveSpec;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * Service that orchestrates data processing
 */
@Log4j2
@RequiredArgsConstructor
public class ProcessService {
    private final ResolveService resolveService;
    private final PostMapService postMapService;
    private final ObjectMapper json;
    private final JsonWalker walker;

    @NotNull
    public Mono<String> process(
            @NotNull final ResolveSpec spec,
            @NotNull final String response) {
        return Mono.fromCallable(() -> json.readTree(response))
                .flatMap(object -> process(spec, object))
                .map(this::writeAsString);
    }

    @NotNull
    private Mono<JsonNode> process(
            @NotNull final ResolveSpec spec,
            @NotNull final JsonNode object) {
        if (!spec.getUseCollection()) { // single resource
            return resolveService.resolve(spec.getId(), (ObjectNode) object, spec.getResolvers())
                    .flatMap(obj -> postMapService.map(obj, spec.getPostMappers()))
                    .onErrorReturn(object);
        } else {
            ArrayNode array = walker.array(object, spec.getCollection());
            return resolveService.resolve(spec.getId(), array, spec.getResolvers())
                    .flatMapMany(Flux::fromIterable)
                    .parallel()
                    .runOn(Schedulers.parallel())
                    .flatMap(obj -> postMapService.map(obj, spec.getPostMappers()))
                    .sequential()
                    .then()
                    .thenReturn(object)
                    .onErrorReturn(object);
        }
    }

    @NotNull
    private String writeAsString(@NotNull final Object object) {
        try {
            return json.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error("Cannot write JSON", e);
            return "";
        }
    }
}
