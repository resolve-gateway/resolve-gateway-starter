package com.vaddya.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.vaddya.resolver.domain.Resolver;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Mono;

@Deprecated
@FunctionalInterface
public interface FieldResolver {
    @NotNull
    Mono<JsonNode> resolve(
            @NotNull final Resolver resolver,
            @NotNull final JsonNode node);
}
