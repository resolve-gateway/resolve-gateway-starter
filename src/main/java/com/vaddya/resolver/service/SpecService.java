package com.vaddya.resolver.service;

import com.vaddya.resolver.domain.ResolveSpec;
import com.vaddya.resolver.repository.PostMappersRepository;
import com.vaddya.resolver.repository.ResolveRoutesRepository;
import com.vaddya.resolver.repository.ResolversRepository;
import com.vaddya.resolver.util.JsonPath;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.gateway.route.Route;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.emptyList;

/**
 * Service that created specification for resolving and post mapping
 * based on the given route and http parameters
 */
@Log4j2
@RequiredArgsConstructor
public class SpecService {
    private final ResolversRepository resolversRepository;
    private final PostMappersRepository postMappersRepository;
    private final ResolveRoutesRepository resolveRoutesRepository;

    @NotNull
    public Mono<ResolveSpec> createSpec(
            @NotNull final Route route,
            @NotNull final List<String> resolversId,
            @NotNull final List<String> postMappersId,
            @Nullable final String collection) {
        return getDefaultSpec(route)
                .switchIfEmpty(Mono.defer(() -> {
                    if (resolversId.isEmpty() && postMappersId.isEmpty()) {
                        log.trace("Neither resolvers nor post-mappers are required for route {}", route::getId);
                        return Mono.empty();
                    }
                    log.trace("Returning empty resolve spec");
                    return emptySpec();
                }))
                .flatMap(spec -> addCollection(spec, collection))
                .flatMap(spec -> collectResolvers(spec, resolversId))
                .flatMap(spec -> collectPostMappers(spec, postMappersId));
    }

    @NotNull
    private Mono<ResolveSpec> getDefaultSpec(@NotNull final Route route) {
        return resolveRoutesRepository.get(route.getId())
                .map(resolveRoute -> ResolveSpec.builder()
                        .id(UUID.randomUUID())
                        .useCollection(resolveRoute.getUseCollection())
                        .collection(resolveRoute.getCollection())
                        .resolvers(resolveRoute.getResolvers())
                        .postMappers(resolveRoute.getPostMappers())
                        .build());
    }

    @NotNull
    private Mono<ResolveSpec> emptySpec() {
        final var spec = ResolveSpec.builder()
                .id(UUID.randomUUID())
                .useCollection(false)
                .collection(JsonPath.empty())
                .resolvers(emptyList())
                .postMappers(emptyList())
                .build();
        return Mono.just(spec);
    }

    @NotNull
    private Mono<ResolveSpec> addCollection(
            @NotNull final ResolveSpec spec,
            @Nullable final String collection) {
        if (collection != null) {
            spec.setCollection(JsonPath.from(collection));
        }
        return Mono.just(spec);
    }

    @NotNull
    private Mono<ResolveSpec> collectResolvers(
            @NotNull final ResolveSpec spec,
            @NotNull final List<String> resolversId) {
        if (resolversId.isEmpty()) {
            log.trace("No resolvers");
            return Mono.just(spec);
        }
        return resolversRepository.getAllByIds(resolversId)
                .collectList()
                .switchIfEmpty(Mono.fromCallable(Collections::emptyList))
                .map(resolvers -> {
                    log.trace("Adding resolvers to spec: {}", resolvers);
                    spec.setResolvers(resolvers);
                    return spec;
                });
    }

    @NotNull
    private Mono<ResolveSpec> collectPostMappers(
            @NotNull final ResolveSpec spec,
            @NotNull final List<String> postMappersId) {
        if (postMappersId.isEmpty()) {
            log.trace("No post mappers");
            return Mono.just(spec);
        }
        return postMappersRepository.getAllByIds(postMappersId)
                .collectList()
                .switchIfEmpty(Mono.fromCallable(Collections::emptyList))
                .map(postMappers -> {
                    log.trace("Adding post mappers to spec: {}", postMappers);
                    spec.setPostMappers(postMappers);
                    return spec;
                });
    }
}
