package com.vaddya.resolver.service.exception;

import org.jetbrains.annotations.NotNull;

public class InvalidJsonPathException extends RuntimeException {

    public InvalidJsonPathException(@NotNull final String message) {
        super(message);
    }
}
