package com.vaddya.resolver.service.exception;

import org.jetbrains.annotations.NotNull;

public class ResolveException extends RuntimeException {

    public ResolveException(@NotNull final String message) {
        super(message);
    }
}
