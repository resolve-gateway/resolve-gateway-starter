package com.vaddya.resolver.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.vaddya.resolver.service.exception.InvalidJsonPathException;
import com.vaddya.resolver.util.JsonPath;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;

/**
 * Service that handles walk request against the given JSON object
 */
@Log4j2
public class JsonWalker {
    @NotNull
    public ObjectNode obj(
            @NotNull final JsonNode root,
            @NotNull final JsonPath path,
            final boolean create) {
        JsonNode node = node(root, path, create);
        if (!node.isObject()) {
            log.error("Node {} is not object in {}", path, root);
            throw new InvalidJsonPathException("Cannot get object: " + path);
        }
        return (ObjectNode) node;
    }

    @NotNull
    public ArrayNode array(
            @NotNull final JsonNode root,
            @NotNull final JsonPath path) {
        JsonNode node = node(root, path, false);
        if (!node.isArray()) {
            log.error("Node {} is not array in {}", path, root);
            throw new InvalidJsonPathException("Cannot get array: " + path);
        }
        return (ArrayNode) node;
    }

    @NotNull
    public JsonNode node(
            @NotNull final JsonNode root,
            @NotNull final JsonPath path,
            final boolean create) {
        if (path.isEmpty()) {
            return root;
        }
        if (root.isMissingNode()) {
            log.error("Empty root: {}", root);
            throw new RuntimeException();
        }
        log.trace("Walking in json, path {} in object {}", path, root);
        JsonNode current = root;
        for (final var field : path) {
            if (!current.has(field)) {
                if (!create) {
                    log.error("Cannot get {} in root object {}", path, root);
                    throw new InvalidJsonPathException("Cannot get field: " + field);
                }
                if (!current.isObject()) {
                    log.error("Cannot create field {} in non-object node {}", field, current);
                    throw new InvalidJsonPathException("Cannot create field: " + field);
                }
                ((ObjectNode) current).putObject(field);
            }
            current = current.get(field);
        }

        return current;
    }
}
