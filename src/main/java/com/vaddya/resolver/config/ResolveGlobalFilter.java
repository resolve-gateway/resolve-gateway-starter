package com.vaddya.resolver.config;

import com.vaddya.resolver.domain.ResolveSpec;
import com.vaddya.resolver.service.ProcessService;
import com.vaddya.resolver.service.SpecService;
import com.vaddya.resolver.util.GzipServerHttpResponseDecorator;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;

/**
 * Global filter to resolve data dependencies
 */
@Log4j2
@RequiredArgsConstructor
public class ResolveGlobalFilter implements GlobalFilter, Ordered {
    private static final String RESOLVERS_HEADER = "Gateway-Resolvers";
    private static final String POST_MAPPERS_HEADER = "Gateway-Post-Mappers";
    private static final String COLLECTION_HEADER = "Gateway-Collection";

    private final ResolveConfig resolveConfig;
    private final ProcessService processService;
    private final SpecService specService;

    @Override
    public int getOrder() {
        return resolveConfig.getOrder();
    }

    @Override
    @NotNull
    public Mono<Void> filter(
            @NotNull final ServerWebExchange exchange,
            @NotNull final GatewayFilterChain chain) {
        // resolving is disabled
        if (!resolveConfig.isEnabled()) {
            return chain.filter(exchange);
        }

        // get gateway headers
        final var headers = exchange.getRequest().getHeaders();
        final var resolvers = getHeaderValues(headers.getFirst(RESOLVERS_HEADER));
        final var postMappers = getHeaderValues(headers.getFirst(POST_MAPPERS_HEADER));
        final var collection = headers.getFirst(COLLECTION_HEADER);

        // remove gateway headers before proxying
        final var request = exchange.getRequest().mutate().headers(h -> {
            h.remove(RESOLVERS_HEADER);
            h.remove(POST_MAPPERS_HEADER);
            h.remove(COLLECTION_HEADER);
        }).build();

        final Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);
        if (route == null) { // request is not routed
            return chain.filter(exchange);
        }
        log.trace("Route: {}", route::getId);

        // create resolve spec
        return specService.createSpec(route, resolvers, postMappers, collection)
                .map(spec -> wrapGzip(exchange.getResponse(), spec))
                .switchIfEmpty(Mono.fromCallable(exchange::getResponse))
                .flatMap(response -> {
                    log.trace("Mutating exchange");
                    final var ex = exchange.mutate()
                            .request(request)
                            .response(response)
                            .build();
                    return chain.filter(ex);
                });
    }

    @NotNull
    private ServerHttpResponse wrapGzip(
            @NotNull final ServerHttpResponse response,
            @NotNull final ResolveSpec spec) {
        log.trace("Spec: {}", spec);
        return new GzipServerHttpResponseDecorator(
                response,
                r -> processService.process(spec, r)
        );
    }

    @NotNull
    private List<String> getHeaderValues(@Nullable final String header) {
        return Optional.ofNullable(header)
                .map(s -> s.split(","))
                .map(Arrays::asList)
                .orElse(emptyList());
    }
}
