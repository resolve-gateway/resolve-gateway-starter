package com.vaddya.resolver.config;

import com.vaddya.resolver.domain.PostMapperProps;
import com.vaddya.resolver.domain.ResolveRouteProps;
import com.vaddya.resolver.domain.ResolverProps;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties("resolve")
@Data
public class ResolveProps {
    private Boolean enabled = Boolean.TRUE;
    private Integer order = -2;
    private List<ResolverProps> resolvers = new ArrayList<>();
    private List<PostMapperProps> postMappers = new ArrayList<>();
    private List<ResolveRouteProps> routes = new ArrayList<>();
}
