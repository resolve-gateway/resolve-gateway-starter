package com.vaddya.resolver.config;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

/**
 * Data class that contains general resolve configuration
 */
@Data
class ResolveConfig {
    private boolean enabled;
    private int order;

    ResolveConfig(@NotNull final ResolveProps resolveProps) {
        this.enabled = resolveProps.getEnabled();
        this.order = resolveProps.getOrder();
    }
}
