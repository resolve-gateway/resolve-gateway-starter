package com.vaddya.resolver.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaddya.resolver.controller.PostMappersController;
import com.vaddya.resolver.controller.ResolveEndpoints;
import com.vaddya.resolver.controller.ResolveRoutesController;
import com.vaddya.resolver.controller.ResolversController;
import com.vaddya.resolver.converter.PostMappersConverter;
import com.vaddya.resolver.converter.ResolveRoutesConverter;
import com.vaddya.resolver.converter.ResolversConverter;
import com.vaddya.resolver.repository.*;
import com.vaddya.resolver.service.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;

@Configuration
@EnableConfigurationProperties(ResolveProps.class)
@ConditionalOnProperty(value = "resolve.enabled", havingValue = "true", matchIfMissing = true)
public class ResolveAutoConfig {
    @Bean
    @ConditionalOnMissingBean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    @Bean
    @ConditionalOnMissingBean
    public WebClient webClient() {
        return WebClient.builder().build();
    }

    @Bean
    public RouterFunction<ServerResponse> resourcesRouter() {
        return RouterFunctions
                .resources("/**", new ClassPathResource("public/"));
    }

    @Bean
    public CorsWebFilter corsFilter() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);

        return new CorsWebFilter(source);
    }

    @Bean
    public ResolveConfig resolveConfig(ResolveProps resolveProps) {
        return new ResolveConfig(resolveProps);
    }

    @Bean
    public ResolveGlobalFilter resolveGlobalFilter(
            ResolveConfig resolveConfig,
            ProcessService processService,
            SpecService specService) {
        return new ResolveGlobalFilter(resolveConfig, processService, specService);
    }

    @Bean
    public ResolversConverter resolversConverter() {
        return new ResolversConverter();
    }

    @Bean
    public PostMappersConverter postMappersConverter() {
        return new PostMappersConverter();
    }

    @Bean
    @ConditionalOnMissingBean
    public RouteLocator routeLocator() {
        return Flux::empty;
    }

    @Bean
    public ResolveRoutesConverter resolveRoutesConverter(
            RouteLocator routeLocator,
            ResolversRepository resolversRepository,
            PostMappersRepository postMappersRepository) {
        return new ResolveRoutesConverter(routeLocator, resolversRepository, postMappersRepository);
    }

    @Bean
    @ConditionalOnMissingBean
    public ResolversRepository resolversRepository(
            ResolveProps resolveProps,
            ResolversConverter resolversConverter) {
        return new InMemoryResolversRepository(resolveProps, resolversConverter);
    }

    @Bean
    @ConditionalOnMissingBean
    public PostMappersRepository postMappersRepository(
            ResolveProps props,
            PostMappersConverter postMappersConverter) {
        return new InMemoryPostMappersRepository(props, postMappersConverter);
    }

    @Bean
    @ConditionalOnMissingBean
    public ResolveRoutesRepository resolveRoutesRepository(
            ResolveProps resolveProps,
            ResolveRoutesConverter resolveRoutesConverter) {
        return new InMemoryResolveRoutesRepository(resolveProps, resolveRoutesConverter);
    }

    @Bean
    public ResolveEndpoints resolveEndpoints(
            ResolversController resolverController,
            PostMappersController postMapperController,
            ResolveRoutesController resolveRouteController) {
        return new ResolveEndpoints(resolverController, postMapperController, resolveRouteController);
    }

    @Bean
    public ResolversController resolverController(
            ResolversRepository resolversRepository,
            ResolversConverter resolversConverter) {
        return new ResolversController(resolversRepository, resolversConverter);
    }

    @Bean
    public PostMappersController postMapperController(PostMappersRepository postMappersRepository,
                                                      PostMappersConverter postMappersConverter) {
        return new PostMappersController(postMappersRepository, postMappersConverter);
    }

    @Bean
    public ResolveRoutesController resolveRouteController(
            ResolveRoutesRepository resolveRoutesRepository,
            ResolveRoutesConverter resolveRoutesConverter) {
        return new ResolveRoutesController(resolveRoutesRepository, resolveRoutesConverter);
    }

    @Bean
    public CacheService cacheService(WebClient webClient) {
        return new CacheService(webClient);
    }

    @Bean
    public HrefFieldResolver hrefFieldResolver(CacheService cacheService) {
        return new HrefFieldResolver(cacheService);
    }

    @Bean
    public UriFieldResolver uriFieldResolver(CacheService cacheService) {
        return new UriFieldResolver(cacheService);
    }

    @Bean
    public JsonWalker jsonWalker() {
        return new JsonWalker();
    }

    @Bean
    public ProcessService processService(
            ResolveService resolveService,
            PostMapService postMapService,
            ObjectMapper objectMapper,
            JsonWalker jsonWalker) {
        return new ProcessService(resolveService, postMapService, objectMapper, jsonWalker);
    }

    @Bean
    public ResolveService resolveService(CacheService cacheService, JsonWalker jsonWalker) {
        return new ResolveService(cacheService, jsonWalker);
    }

    @Bean
    public PostMapService postMapService(
            JsonWalker jsonWalker,
            ObjectMapper objectMapper) {
        return new PostMapService(jsonWalker, objectMapper);
    }

    @Bean
    public SpecService specService(
            ResolversRepository resolversRepository,
            PostMappersRepository postMappersRepository,
            ResolveRoutesRepository resolveRoutesRepository) {
        return new SpecService(resolversRepository, postMappersRepository, resolveRoutesRepository);
    }
}
