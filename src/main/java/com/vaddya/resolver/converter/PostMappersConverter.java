package com.vaddya.resolver.converter;

import com.vaddya.resolver.domain.PostMapper;
import com.vaddya.resolver.domain.PostMapperProps;
import com.vaddya.resolver.util.JsonPath;
import org.jetbrains.annotations.NotNull;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import reactor.core.publisher.Mono;

/**
 * PostMapper-to-PostMapperProps and vice versa converter
 */
public class PostMappersConverter implements EntityConverter<PostMapper, PostMapperProps> {
    private final ExpressionParser parser = new SpelExpressionParser();
    private final ParserContext context = new TemplateParserContext();

    @Override
    @NotNull
    public Mono<PostMapper> fromProps(@NotNull final PostMapperProps props) {
        return Mono.just(
                PostMapper.builder()
                        .id(props.getId())
                        .expression(parser.parseExpression(props.getExpression(), context))
                        .result(JsonPath.from(props.getResult()))
                        .build());
    }

    @Override
    @NotNull
    public Mono<PostMapperProps> toProps(@NotNull final PostMapper postMapper) {
        return Mono.just(new PostMapperProps(
                postMapper.getId(),
                postMapper.getExpression().getExpressionString(),
                postMapper.getResult().toString()
        ));
    }
}
