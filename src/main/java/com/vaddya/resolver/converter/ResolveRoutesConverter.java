package com.vaddya.resolver.converter;

import com.vaddya.resolver.domain.PostMapper;
import com.vaddya.resolver.domain.ResolveRoute;
import com.vaddya.resolver.domain.ResolveRouteProps;
import com.vaddya.resolver.domain.Resolver;
import com.vaddya.resolver.repository.PostMappersRepository;
import com.vaddya.resolver.repository.ResolversRepository;
import com.vaddya.resolver.util.JsonPath;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.cloud.gateway.route.RouteLocator;
import reactor.core.publisher.Mono;

import static java.util.stream.Collectors.toList;

/**
 * ResolveRoute-to-ResolveRouteProps and vice versa converter
 */
@RequiredArgsConstructor
public class ResolveRoutesConverter implements EntityConverter<ResolveRoute, ResolveRouteProps> {
    private final RouteLocator routeLocator;
    private final ResolversRepository resolversRepository;
    private final PostMappersRepository postMappersRepository;

    @Override
    @NotNull
    public Mono<ResolveRoute> fromProps(@NotNull final ResolveRouteProps props) {
        return routeLocator.getRoutes()
                .filter(route -> route.getId().equals(props.getId()))
                .next()
                .flatMap(route -> {
                    final var resolvers = resolversRepository
                            .getAllByIds(props.getResolvers())
                            .collectList();
                    final var postMappers = postMappersRepository
                            .getAllByIds(props.getPostMappers())
                            .collectList();

                    return Mono.zip(resolvers, postMappers)
                            .map(tuple -> ResolveRoute.builder()
                                    .route(route)
                                    .useCollection(props.getUseCollection())
                                    .collection(JsonPath.from(props.getCollection()))
                                    .resolvers(tuple.getT1())
                                    .postMappers(tuple.getT2())
                                    .build());
                })
                .switchIfEmpty(Mono.error(new IllegalArgumentException(props.getId())));
    }

    @Override
    public Mono<ResolveRouteProps> toProps(ResolveRoute resolveRoute) {
        return Mono.just(new ResolveRouteProps(
                resolveRoute.getRoute().getId(),
                resolveRoute.getUseCollection(),
                resolveRoute.getCollection().toString(),
                resolveRoute.getResolvers().stream().map(Resolver::getId).collect(toList()),
                resolveRoute.getPostMappers().stream().map(PostMapper::getId).collect(toList())
        ));
    }
}
