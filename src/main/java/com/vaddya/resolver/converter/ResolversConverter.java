package com.vaddya.resolver.converter;

import com.vaddya.resolver.domain.Resolver;
import com.vaddya.resolver.domain.ResolverProps;
import com.vaddya.resolver.util.JsonPath;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import reactor.core.publisher.Mono;

import java.net.URI;

/**
 * Resolver-to-ResolverProps and vice versa converter
 */
public class ResolversConverter implements EntityConverter<Resolver, ResolverProps> {
    @Override
    @NotNull
    public Mono<Resolver> fromProps(@NotNull final ResolverProps props) {
        return Mono.just(
                Resolver.builder()
                        .id(props.getId())
                        .source(sourceFromProps(props.getSource()))
                        .result(jsonPathFromString(props.getResult()))
                        .build()
        );
    }

    @Override
    @NotNull
    public Mono<ResolverProps> toProps(@NotNull final Resolver resolver) {
        return Mono.just(
                ResolverProps.builder()
                        .id(resolver.getId())
                        .source(sourceToProps(resolver.getSource()))
                        .result(jsonPathToString(resolver.getResult()))
                        .build()
        );
    }

    @Nullable
    private Resolver.Source sourceFromProps(@Nullable final ResolverProps.Source props) {
        return props == null
                ? null
                : Resolver.Source.builder()
                .field(JsonPath.from(props.getField()))
                .uri(uriFromString(props.getUri()))
                .batch(batchFromProps(props.getBatch()))
                .build();
    }

    @Nullable
    private ResolverProps.Source sourceToProps(@Nullable final Resolver.Source source) {
        return source == null
                ? null
                : ResolverProps.Source.builder()
                .field(jsonPathToString(source.getField()))
                .uri(uriToString(source.getUri()))
                .batch(batchToProps(source.getBatch()))
                .build();
    }

    @Nullable
    private Resolver.Source.Batch batchFromProps(@Nullable final ResolverProps.Source.Batch props) {
        return props == null
                ? null
                : Resolver.Source.Batch.builder()
                .uri(uriFromString(props.getUri()))
                .collection(jsonPathFromString(props.getCollection()))
                .query(props.getQuery())
                .delimiter(props.getDelimiter())
                .build();
    }

    @Nullable
    private ResolverProps.Source.Batch batchToProps(@Nullable final Resolver.Source.Batch batch) {
        return batch == null
                ? null
                : ResolverProps.Source.Batch.builder()
                .uri(uriToString(batch.getUri()))
                .collection(jsonPathToString(batch.getCollection()))
                .query(batch.getQuery())
                .delimiter(batch.getDelimiter())
                .build();

    }

    @Nullable
    private static URI uriFromString(@Nullable final String uri) {
        return uri == null
                ? null
                : URI.create(uri);
    }

    @Nullable
    private String uriToString(@Nullable final URI uri) {
        return uri == null
                ? null
                : uri.toString();
    }

    @Nullable
    private JsonPath jsonPathFromString(@Nullable final String path) {
        return path == null
                ? null
                : JsonPath.from(path);
    }

    @Nullable
    private String jsonPathToString(@Nullable final JsonPath path) {
        return path == null
                ? null
                : path.toString();
    }
}
