package com.vaddya.resolver.converter;

import reactor.core.publisher.Mono;

/**
 * Entity-to-props and vice versa converter
 */
public interface EntityConverter<E, P> {
    /**
     * Convert props to entity
     */
    Mono<E> fromProps(P props);

    default Mono<E> fromProps(Mono<P> props) {
        return props.flatMap(this::fromProps);
    }

    /**
     * Convert entity to props
     */
    Mono<P> toProps(E entity);

    default Mono<P> toProps(Mono<E> entity) {
        return entity.flatMap(this::toProps);
    }
}
