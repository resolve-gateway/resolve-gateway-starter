package com.vaddya.resolver.util;

import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Util class than represents the path inside JSON object
 */
@EqualsAndHashCode
public class JsonPath implements Iterable<String> {
    private final String source;
    private final List<String> path;

    public static JsonPath from(String source) {
        return new JsonPath(source);
    }

    public static JsonPath empty() {
        return new JsonPath("");
    }

    private JsonPath(@Nullable String source) {
        if (source == null) {
            source = "";
        }
        if (source.contains(" ")) {
            throw new IllegalArgumentException("Unable to create JsonPath from " + source);
        }

        this.source = source;
        this.path = Arrays.stream(source.split("\\."))
                .filter(it -> !it.isEmpty())
                .collect(Collectors.toList());
    }

    @NotNull
    public String get(final int index) {
        return path.get(index);
    }

    @NotNull
    public String last() {
        if (path.isEmpty()) {
            throw new IllegalArgumentException("Called last() on empty JsonPath");
        }
        return path.get(path.size() - 1);
    }

    @NotNull
    public String first() {
        if (path.isEmpty()) {
            throw new IllegalArgumentException("Called first() on empty JsonPath");
        }
        return path.get(0);
    }

    @NotNull
    public JsonPath add(@NotNull final String path) {
        return new JsonPath(source + "." + path);
    }

    @Override
    @NotNull
    public String toString() {
        return source;
    }

    public int size() {
        return path.size();
    }

    public boolean isEmpty() {
        return path.isEmpty();
    }

    @NotNull
    public JsonPath withoutLast() {
        int index = source.lastIndexOf(".");
        if (index == -1) {
            return JsonPath.empty();
        }
        String newSource = source.substring(0, index);
        return new JsonPath(newSource);
    }

    @NotNull
    public JsonPath withoutFirst() {
        int index = source.indexOf(".");
        if (index == -1) {
            return JsonPath.empty();
        }
        String newSource = source.substring(index + 1);
        return new JsonPath(newSource);
    }

    @Override
    @NotNull
    public Iterator<String> iterator() {
        return path.iterator();
    }
}
