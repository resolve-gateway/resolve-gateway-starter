package com.vaddya.resolver.util;

import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.function.Function;

import static com.vaddya.resolver.util.GzipUtils.decodeGzip;
import static com.vaddya.resolver.util.GzipUtils.encodeGzip;

/**
 * ServerHttpResponse decorator that decodes/encodes data from/to gzipped
 * bytes if those operations are required
 */
@Log4j2
public class GzipServerHttpResponseDecorator extends ServerHttpResponseDecorator {
    private final Function<String, Mono<String>> function;

    public GzipServerHttpResponseDecorator(@NotNull final ServerHttpResponse response,
                                           @NotNull final Function<String, Mono<String>> function) {
        super(response);
        this.function = function;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NotNull
    public Mono<Void> writeWith(@NotNull final Publisher<? extends DataBuffer> body) {
        final var isGzipped = isGzipped(getDelegate());
        final var bufferFactory = getDelegate().bufferFactory();
        if (body instanceof Flux) {
            Flux<DataBuffer> flux = (Flux<DataBuffer>) body;
            return super.writeWith(
                    DataBufferUtils.join(flux)
                            .map(DataBuffer::asByteBuffer)
                            .map(GzipServerHttpResponseDecorator::byteBufferToBytes)
                            .map(bytes -> isGzipped ? decodeGzip(bytes) : bytes)
                            .map(bytes -> new String(bytes, StandardCharsets.UTF_8))
                            .flatMap(response -> {
                                try {
                                    return function.apply(response);
                                } catch (Exception e) {
                                    log.error("Function error", e);
                                    return Mono.just(response);
                                }
                            })
                            .map(String::getBytes)
                            .map(bytes -> isGzipped ? encodeGzip(bytes) : bytes)
                            .map(bufferFactory::wrap));
        }
        return super.writeWith(body);
    }

    private static byte[] byteBufferToBytes(@NotNull final ByteBuffer buffer) {
        if (buffer.hasArray()) {
            return buffer.array();
        } else {
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            return bytes;
        }
    }

    private static boolean isGzipped(@NotNull final ServerHttpResponse response) {
        return Optional.of(response.getHeaders())
                .map(headers -> headers.get(HttpHeaders.CONTENT_ENCODING))
                .map(encoding -> encoding.contains("gzip"))
                .orElse(false);
    }
}