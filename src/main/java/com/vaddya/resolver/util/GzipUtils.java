package com.vaddya.resolver.util;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Util class that handles encoding/decoding to/from gzip
 */
public class GzipUtils {
    @NotNull
    public static byte[] encodeGzip(@NotNull final byte[] data) {
        try {
            var out = new ByteArrayOutputStream();
            var gzip = new GZIPOutputStream(out);
            gzip.write(data);
            gzip.finish();
            gzip.close();
            return out.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @NotNull
    public static byte[] decodeGzip(@NotNull final byte[] data) {
        try {
            InputStream stream = new ByteArrayInputStream(data);
            return new GZIPInputStream(stream).readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
