package com.vaddya.resolver.repository;

import com.vaddya.resolver.domain.ResolveRoute;

/**
 * Repository to store Resolve Routes
 */
public interface ResolveRoutesRepository extends EntityRepository<ResolveRoute> {
}
