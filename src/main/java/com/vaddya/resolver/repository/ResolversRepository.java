package com.vaddya.resolver.repository;

import com.vaddya.resolver.domain.Resolver;

/**
 * Repository to store Resolvers
 */
public interface ResolversRepository extends EntityRepository<Resolver> {
}
