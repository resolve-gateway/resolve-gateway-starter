package com.vaddya.resolver.repository;

import com.vaddya.resolver.domain.PostMapper;

/**
 * Repository to store Post Mappers
 */
public interface PostMappersRepository extends EntityRepository<PostMapper> {
}
