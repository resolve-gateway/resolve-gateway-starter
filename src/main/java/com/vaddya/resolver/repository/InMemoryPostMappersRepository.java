package com.vaddya.resolver.repository;

import com.vaddya.resolver.config.ResolveProps;
import com.vaddya.resolver.converter.PostMappersConverter;
import com.vaddya.resolver.domain.PostMapper;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toConcurrentMap;

/**
 * In-memory Post Mappers repository
 */
public class InMemoryPostMappersRepository implements PostMappersRepository {
    private final Map<String, PostMapper> postMappers;

    public InMemoryPostMappersRepository(
            @NotNull final ResolveProps properties,
            @NotNull final PostMappersConverter converter) {
        this.postMappers = properties.getPostMappers()
                .stream()
                .map(converter::fromProps)
                .map(Mono::block)
                .filter(Objects::nonNull)
                .collect(toConcurrentMap(PostMapper::getId, identity()));
    }

    @Override
    @NotNull
    public Mono<PostMapper> get(@NotNull final String id) {
        if (!postMappers.containsKey(id)) {
            return Mono.error(new NoSuchElementException(id));
        }
        return Mono.just(postMappers.get(id));
    }

    @Override
    @NotNull
    public Flux<PostMapper> getAll() {
        return Flux.fromIterable(postMappers.values());
    }

    @Override
    @NotNull
    public Flux<PostMapper> getAllByIds(@NotNull final Iterable<String> ids) {
        return Flux.fromIterable(ids)
                .filter(postMappers::containsKey)
                .map(postMappers::get);
    }

    @Override
    @NotNull
    public Mono<PostMapper> put(@NotNull final PostMapper postMapper) {
        postMappers.put(postMapper.getId(), postMapper);
        return Mono.just(postMapper);
    }

    @Override
    @NotNull
    public Mono<Void> remove(@NotNull final String id) {
        postMappers.remove(id);
        return Mono.empty();
    }
}
