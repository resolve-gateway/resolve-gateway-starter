package com.vaddya.resolver.repository;

import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Repository that handles get/put/remove operations on entities of type E
 */
public interface EntityRepository<E> {
    /**
     * Get entity by ID
     */
    Mono<E> get(@NotNull String id);

    /**
     * Get all entities
     */
    Flux<E> getAll();

    /**
     * Get entries by IDs
     */
    Flux<E> getAllByIds(@NotNull Iterable<String> ids);

    /**
     * Create or update entity by ID
     */
    Mono<E> put(@NotNull E entity);

    /**
     * Delete entity by ID
     */
    Mono<Void> remove(@NotNull String id);
}
