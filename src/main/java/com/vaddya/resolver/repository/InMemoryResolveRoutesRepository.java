package com.vaddya.resolver.repository;

import com.vaddya.resolver.config.ResolveProps;
import com.vaddya.resolver.converter.ResolveRoutesConverter;
import com.vaddya.resolver.domain.ResolveRoute;
import com.vaddya.resolver.domain.ResolveRouteProps;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * In-memory Resolve Routes repository
 */
public class InMemoryResolveRoutesRepository implements ResolveRoutesRepository {
    private final Map<String, ResolveRouteProps> resolveRoutes;
    private final ResolveRoutesConverter converter;

    public InMemoryResolveRoutesRepository(
            @NotNull final ResolveProps properties,
            @NotNull final ResolveRoutesConverter converter) {
        this.converter = converter;
        this.resolveRoutes = properties.getRoutes()
                .stream()
                .collect(toMap(ResolveRouteProps::getId, identity()));
    }

    @Override
    public Mono<ResolveRoute> get(@NotNull final String id) {
        return Mono.justOrEmpty(resolveRoutes.get(id))
                .flatMap(converter::fromProps);
    }

    @Override
    public Flux<ResolveRoute> getAll() {
        return Flux.fromIterable(resolveRoutes.values())
                .flatMap(converter::fromProps);
    }

    @Override
    public Flux<ResolveRoute> getAllByIds(@NotNull final Iterable<String> ids) {
        return Flux.fromIterable(ids)
                .flatMap(this::get);
    }

    @Override
    public Mono<ResolveRoute> put(@NotNull final ResolveRoute resolveRoute) {
        return converter.toProps(resolveRoute)
                .map(props -> {
                    resolveRoutes.put(props.getId(), props);
                    return props;
                })
                .thenReturn(resolveRoute);
    }

    public Mono<ResolveRouteProps> put(@NotNull final ResolveRouteProps props) {
        resolveRoutes.put(props.getId(), props);
        return Mono.just(props);
    }

    @Override
    public Mono<Void> remove(@NotNull final String id) {
        resolveRoutes.remove(id);
        return Mono.empty();
    }
}
