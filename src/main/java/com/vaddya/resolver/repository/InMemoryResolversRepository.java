package com.vaddya.resolver.repository;

import com.vaddya.resolver.config.ResolveProps;
import com.vaddya.resolver.converter.ResolversConverter;
import com.vaddya.resolver.domain.Resolver;
import org.jetbrains.annotations.NotNull;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toConcurrentMap;

/**
 * In-memory Resolvers repository
 */
public class InMemoryResolversRepository implements ResolversRepository {
    private final Map<String, Resolver> resolvers;

    public InMemoryResolversRepository(
            @NotNull final ResolveProps properties,
            @NotNull final ResolversConverter converter) {
        this.resolvers = properties.getResolvers()
                .stream()
                .map(converter::fromProps)
                .map(Mono::block)
                .filter(Objects::nonNull)
                .collect(toConcurrentMap(Resolver::getId, identity()));
    }

    @Override
    @NotNull
    public Mono<Resolver> get(@NotNull final String id) {
        if (!resolvers.containsKey(id)) {
            return Mono.error(new NoSuchElementException(id));
        }
        return Mono.just(resolvers.get(id));
    }

    @Override
    @NotNull
    public Flux<Resolver> getAll() {
        return Flux.fromIterable(resolvers.values());
    }

    @Override
    @NotNull
    public Flux<Resolver> getAllByIds(@NotNull final Iterable<String> ids) {
        return Flux.fromIterable(ids)
                .filter(resolvers::containsKey)
                .map(resolvers::get);
    }

    @Override
    @NotNull
    public Mono<Resolver> put(@NotNull final Resolver resolver) {
        resolvers.put(resolver.getId(), resolver);
        return Mono.just(resolver);
    }

    @Override
    @NotNull
    public Mono<Void> remove(@NotNull final String id) {
        resolvers.remove(id);
        return Mono.empty();
    }
}
