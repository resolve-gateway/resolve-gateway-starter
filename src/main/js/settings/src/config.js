const base = process.env.REACT_APP_STAGE === 'production' ? '' : 'http://localhost:8080';

export default {
    api: {
        resolvers: base + '/actuator/resolve/resolvers',
        postMappers: base + '/actuator/resolve/postmappers',
        resolveRoutes: base + '/actuator/resolve/routes',
        gatewayRoutes: base + '/actuator/gateway/routes',
        gatewayRefresh: base + '/actuator/gateway/refresh'
    }
};
