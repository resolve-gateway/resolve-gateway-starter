import React from 'react';
import {Alert, Form, Modal} from 'react-bootstrap';
import ModalFooterButtons from "../ModalFooterButtons";

export default class PostMapperModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            status: {
                create: false,
                show: false,
                error: null
            },
            fields: {
                postMapperId: '',
                postMapperExpression: '',
                postMapperResult: ''
            },
            callbacks: {
                onSave: null,
                onRemove: null
            }
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.save = this.save.bind(this);
        this.remove = this.remove.bind(this);

        this.onTextInputChange = this.onTextInputChange.bind(this);
    }

    open(postMapper, onSave, onRemove) {
        this.setState({
            status: {
                ...this.state.status,
                create: postMapper == null,
                show: true
            },
            fields: {
                postMapperId: (postMapper && postMapper.id) || '',
                postMapperExpression: (postMapper && postMapper.expression) || '',
                postMapperResult: (postMapper && postMapper.result) || ''
            },
            callbacks: {
                onSave: onSave,
                onRemove: onRemove
            }
        });
    }

    close() {
        this.setState({
            status: {
                ...this.state.status,
                show: false
            }
        });
    }

    save() {
        this.state.callbacks.onSave && this.state.callbacks.onSave({
            id: this.state.fields.postMapperId,
            expression: this.state.fields.postMapperExpression,
            result: this.state.fields.postMapperResult
        });
        this.close();
    }

    remove() {
        this.state.callbacks.onRemove && this.state.callbacks.onRemove();
        this.close();
    }

    onTextInputChange(e) {
        const value = e.target.value;
        const id = e.target.id;

        this.setState({
            fields: {
                ...this.state.fields,
                [id]: value
            }
        });
    }

    render() {
        return (
            <Modal
                className='post-mapper-modal'
                size='lg'
                show={this.state.status.show}
                onHide={this.close}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        {this.state.status.create ? 'Create ' : 'Update '}
                        Post Mapper
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.state.status.error &&
                    <Alert variant="danger">
                        <Alert.Heading>Error</Alert.Heading>
                        <p>{this.state.status.error}</p>
                    </Alert>
                    }
                    <Form>
                        <Form.Group controlId='postMapperId'>
                            <Form.Label>
                                Post Mapper ID
                            </Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='E.g. "summaryPostMapper"'
                                value={this.state.fields.postMapperId}
                                onChange={this.onTextInputChange}
                            />
                        </Form.Group>
                        <Form.Group controlId='postMapperExpression'>
                            <Form.Label>
                                Expression (using{' '}
                                <a href="https://docs.spring.io/spring/docs/4.3.10.RELEASE/spring-framework-reference/html/expressions.html"
                                   rel="noopener noreferrer"
                                   target='_blank'
                                >
                                    Spring SpEL
                                </a>{' '} Syntax)
                            </Form.Label>
                            <Form.Control
                                as='textarea'
                                placeholder='E.g. "#{_embedded.person} + #{document.title}"'
                                value={this.state.fields.postMapperExpression}
                                onChange={this.onTextInputChange}
                            />
                        </Form.Group>
                        <Form.Group controlId='postMapperResult'>
                            <Form.Label>
                                Result Field
                            </Form.Label>
                            <Form.Control
                                type='text'
                                placeholder='E.g. "_embedded.summary"'
                                value={this.state.fields.postMapperResult}
                                onChange={this.onTextInputChange}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <ModalFooterButtons
                    isCreate={this.state.status.create}
                    onSave={this.save}
                    onClose={this.close}
                    onRemove={this.remove}
                />
            </Modal>
        );
    }
}
