import React from 'react';
import PropTypes from 'prop-types';
import {Button, Table} from 'react-bootstrap';
import PostMapperModal from './PostMapperModal';

export default class PostMappersList extends React.Component {
    constructor(props) {
        super(props);

        this.onUpdateClick = this.onUpdateClick.bind(this);
        this.onCreateClick = this.onCreateClick.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
    }

    onCreateClick() {
        this.modal.open(
            null,
            (newResolver) => {
                this.props.onCreate(newResolver);
            },
            null
        )
    }

    onUpdateClick(postMapper) {
        this.modal.open(
            postMapper,
            (newPostMapper) => {
                this.props.onUpdate(postMapper.id, newPostMapper);
            },
            () => {
                this.props.onDelete(postMapper.id);
            }
        );
    }

    onDeleteClick(postMapper) {
        this.props.onDelete(postMapper.id);
    }

    render() {
        return (
            <>
                <h2 id='postmappers'>
                    Post Mappers
                    <Button
                        variant='outline-success'
                        className='float-right'
                        onClick={this.onCreateClick}
                    >
                        <i className='fas fa-fw fa-plus'/> Create Post Mapper
                    </Button>
                </h2>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th style={{width: '20%'}}>Post Mapper ID</th>
                        <th style={{width: '50%'}}>Expression</th>
                        <th style={{width: '30%'}}>Result field</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.postMappers && this.props.postMappers.map(postMapper => (
                        <tr
                            key={postMapper.id}
                            className='cursor-pointer'
                            onClick={() => this.onUpdateClick(postMapper)}
                        >
                            <td>
                                {postMapper.id}
                            </td>
                            <td>
                                <code>{postMapper.expression}</code>
                            </td>
                            <td>
                                <code>{postMapper.result}</code>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>

                <PostMapperModal ref={modal => this.modal = modal}/>
            </>
        )
    }
}

PostMappersList.propTypes = {
    postMappers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        expression: PropTypes.string,
        result: PropTypes.string
    })).isRequired,
    onCreate: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired
};
