import React from 'react';
import PostMappersList from './PostMappersList';
import {shallow} from 'enzyme';

describe('PostMappersList', () => {

    it('Should render post mapper', () => {
        const postMapper = {
            id: 'testId',
            expression: 'testExpression',
            result: 'testResult'
        };
        const wrapper = shallow(
            <PostMappersList
                postMappers={[postMapper]}
                onCreate={() => 0}
                onUpdate={() => 0}
                onDelete={() => 0}
            />
        );
        const expression = <code>{postMapper.expression}</code>;
        expect(wrapper).toContainReact(expression);
    });
});
