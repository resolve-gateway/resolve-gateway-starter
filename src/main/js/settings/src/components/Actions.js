import React from 'react';
import {Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function Actions(props) {
    return (
        <>
            <Button
                variant='outline-primary'
                size='sm'
                onClick={props.onUpdateClick}
            >
                <i className='fas fa-sm fa-pen'/>
            </Button>
            {' '}
            <Button
                variant='outline-danger'
                size='sm'
                onClick={props.onDeleteClick}
            >
                <i className='fas fa-sm fa-trash-alt'/>
            </Button>
        </>
    )
}

Actions.propTypes = {
    onUpdateClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired
};
