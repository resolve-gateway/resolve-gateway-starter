import React from "react";

export default (props) => (
    <a href={props.uri}
       title={props.uri}
       target='_blank'
       rel='noopener noreferrer'>
        {props.uri ? props.uri : '—'}
    </a>
)
