import React from 'react';
import Actions from './Actions';
import {shallow} from 'enzyme';

describe('Action buttons', () => {

    it('Should render icons', () => {
        const wrapper = shallow(
            <Actions
                onDeleteClick={() => 0}
                onUpdateClick={() => 0}
            />
        );
        const pen = <i className='fas fa-sm fa-pen'/>;
        const trash = <i className='fas fa-sm fa-trash-alt'/>;
        expect(wrapper).toContainReact(pen);
        expect(wrapper).toContainReact(trash);
    });
});
