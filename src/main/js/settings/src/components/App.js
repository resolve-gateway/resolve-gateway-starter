import React from 'react';
import {connect} from 'react-redux';
import JsYaml from 'js-yaml';
import NavBar from './NavBar';
import ResolversList from './Resolvers/ResolversList';
import PostMappersList from './PostMappers/PostMappersList';
import GatewayRoutesList from './GatewayRoutes/GatewayRoutesList';
import ExportModal from "./ExportModal";
import {createResolver, deleteResolver, retrieveResolvers, updateResolver} from '../store/resolvers/actions';
import {
    createPostMapper,
    deletePostMapperById,
    retrievePostMappers,
    updatePostMapper
} from '../store/postMappers/actions';
import {retrieveResolveRoutes} from "../store/resolveRoutes/actions";
import 'highlight.js/styles/github.css';
import './Utils/Utils.sass';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.export = this.export.bind(this);
    }

    componentDidMount() {
        this.props.retrieveResolvers();
        this.props.retrievePostMappers();
        this.props.retrieveResolveRoutes();
    }

    export() {
        const props = {
            spring: {
                cloud: {
                    gateway: {
                        routes: this.props.gatewayRoutes.map(route => {
                            const value = {
                                ...route,
                                predicates: route.predicates.map(p => `${p.name}=${p.args}`),
                                filters: route.filters.map(f => `${f.name}=${f.args}`)
                            };
                            route.predicates.length === 0 && delete value.predicates;
                            route.filters.length === 0 && delete value.filters;
                            route.order === 0 && delete value.order;
                            return value;
                        })
                    }
                }
            },
            resolve: {
                resolvers: this.props.resolvers.map(resolver => {
                    resolver.source.uri === null && delete resolver.source.uri;
                    resolver.source.batch === null && delete resolver.source.batch;
                    return resolver;
                }),
                postMappers: this.props.postMappers,
                routes: this.props.resolveRoutes.map(route => {
                    const value = {
                        ...route
                    };
                    route.resolvers.length === 0 && delete value.resolvers;
                    route.postMappers.length === 0 && delete value.postMappers;
                    route.useCollection === false && delete value.collection;
                    route.useCollection === false && delete value.useCollection;
                    return value;
                })
            }
        };

        this.props.resolvers.length === 0 && delete props.resolve.resolvers;
        this.props.postMappers.length === 0 && delete props.resolve.postMappers;

        const yaml = JsYaml.dump(props, {
            noCompatMode: true,
            lineWidth: 80
        });
        this.exportModal.open(yaml)
    }

    render() {
        return (
            <>
                <NavBar
                    onExport={this.export}
                />
                <hr/>
                <GatewayRoutesList/>
                <hr/>
                <ResolversList
                    resolvers={this.props.resolvers}
                    onCreate={this.props.createResolver}
                    onUpdate={this.props.updateResolver}
                    onDelete={this.props.deleteResolver}
                />
                <hr/>
                <PostMappersList
                    postMappers={this.props.postMappers}
                    onCreate={this.props.createPostMapper}
                    onUpdate={this.props.updatePostMapper}
                    onDelete={this.props.deletePostMapperById}
                />
                <ExportModal ref={modal => this.exportModal = modal}/>
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    gatewayRoutes: state.gatewayRoutes.list,
    resolveRoutes: state.resolveRoutes.list,
    resolvers: state.resolvers.list,
    postMappers: state.postMappers.list,
});

const mapDispatchToProps = ({
    createResolver,
    retrieveResolvers,
    updateResolver,
    deleteResolver,
    createPostMapper,
    retrievePostMappers,
    updatePostMapper,
    deletePostMapperById,
    retrieveResolveRoutes
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
