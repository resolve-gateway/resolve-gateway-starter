import React from 'react';
import {Modal} from 'react-bootstrap';
import Highlight from 'react-highlight';

export default class ExportModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            show: false,
            text: ''
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
    }

    open(text) {
        this.setState({
            show: true,
            text: text
        });
    }

    close() {
        this.setState({
            show: false,
            text: ''
        });
    }

    render() {
        return (
            <Modal
                className='post-mapper-modal'
                size='lg'
                show={this.state.show}
                onHide={this.close}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Export YAML Configuration
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Highlight className='yaml force-select-all'>
                        {this.state.text}
                    </Highlight>
                </Modal.Body>
            </Modal>
        );
    }
}
