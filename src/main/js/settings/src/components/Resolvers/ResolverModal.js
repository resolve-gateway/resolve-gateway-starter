import React from 'react';
import {Alert, Col, Form, Modal} from 'react-bootstrap';
import ModalFooterButtons from "../ModalFooterButtons";

export default class ResolverModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            status: {
                create: false,
                show: false,
                error: null
            },
            fields: {
                resolverId: '',
                resolverSourceUri: '',
                resolverSourceField: '',
                resolverSourceBatchUri: '',
                resolverSourceBatchCollection: '',
                resolverSourceBatchQuery: '',
                resolverSourceBatchDelimiter: '',
                resolverResult: ''
            },
            onSave: null
        };

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.save = this.save.bind(this);
        this.remove = this.remove.bind(this);

        this.onTextInputChange = this.onTextInputChange.bind(this);
    }

    open(resolver, onSave) {
        this.setState({
            status: {
                ...this.state.status,
                create: resolver == null,
                show: true
            },
            fields: {
                resolverId: (resolver && resolver.id) || '',
                resolverSourceUri: (resolver && resolver.source.uri) || '',
                resolverSourceField: (resolver && resolver.source.field) || '',
                resolverSourceBatchUri: (resolver && resolver.source.batch && resolver.source.batch.uri) || '',
                resolverSourceBatchCollection: (resolver && resolver.source.batch && resolver.source.batch.collection) || '',
                resolverSourceBatchQuery: (resolver && resolver.source.batch && resolver.source.batch.query) || '',
                resolverSourceBatchDelimiter: (resolver && resolver.source.batch && resolver.source.batch.delimiter) || '',
                resolverResult: (resolver && resolver.result) || ''
            },
            onSave: onSave,
        });
    }

    close() {
        this.setState({
            ...this.state,
            status: {
                ...this.state.status,
                show: false
            },
            onSave: null
        });
    }

    save() {
        const resolver = {
            id: this.state.fields.resolverId,
            source: {
                field: this.state.fields.resolverSourceField
            },
            result: this.state.fields.resolverResult
        };
        if (this.state.fields.resolverSourceUri !== '') {
            resolver.source.uri = this.state.fields.resolverSourceUri;
        }
        if (this.state.fields.resolverSourceBatchUri !== '' ||
            this.state.fields.resolverSourceBatchCollection !== '' ||
            this.state.fields.resolverSourceBatchQuery !== '' ||
            this.state.fields.resolverSourceBatchDelimiter !== '') {
            resolver.source.batch = {
                uri: this.state.fields.resolverSourceBatchUri,
                collection: this.state.fields.resolverSourceBatchCollection,
                query: this.state.fields.resolverSourceBatchQuery,
                delimiter: this.state.fields.resolverSourceBatchDelimiter
            };
        }
        this.state.onSave && this.state.onSave(resolver);
        this.close();
    }


    remove() {
        this.state.callbacks.onRemove && this.state.callbacks.onRemove();
        this.close();
    }

    onTextInputChange(e) {
        const value = e.target.value;
        const id = e.target.id;

        this.setState({
            fields: {
                ...this.state.fields,
                [id]: value
            }
        });
    }

    render() {
        return (
            <Modal
                className='resolver-modal'
                size='lg'
                show={this.state.status.show}
                onHide={this.close}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        {this.state.status.create ? 'Create ' : 'Update '}
                        Resolver
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.state.status.error &&
                    <Alert variant="danger">
                        <Alert.Heading>Error</Alert.Heading>
                        <p>{this.state.status.error}</p>
                    </Alert>
                    }
                    <Form>
                        <Form.Group controlId='resolverId'>
                            <Form.Label>
                                Resolver ID
                            </Form.Label>
                            <Form.Control
                                type='text'
                                value={this.state.fields.resolverId}
                                onChange={this.onTextInputChange}
                            />
                        </Form.Group>
                        <Form.Group controlId='resolverSourceUri'>
                            <Form.Label>
                                Resolve URI
                            </Form.Label>
                            <Form.Control
                                type='text'
                                value={this.state.fields.resolverSourceUri}
                                onChange={this.onTextInputChange}
                            />
                        </Form.Group>
                        <Form.Group controlId='resolverSourceField'>
                            <Form.Label>
                                Source Field
                            </Form.Label>
                            <Form.Control
                                type='text'
                                value={this.state.fields.resolverSourceField}
                                onChange={this.onTextInputChange}
                            />
                        </Form.Group>
                        <Form.Row>
                            <Form.Group
                                as={Col} sm={5}
                                controlId='resolverSourceBatchUri'
                            >
                                <Form.Label>
                                    Batch URI
                                </Form.Label>
                                <Form.Control
                                    type='text'
                                    value={this.state.fields.resolverSourceBatchUri}
                                    onChange={this.onTextInputChange}
                                />
                            </Form.Group>
                            <Form.Group
                                as={Col} sm={5}
                                controlId='resolverSourceBatchCollection'
                            >
                                <Form.Label>
                                    Collection
                                </Form.Label>
                                <Form.Control
                                    type='text'
                                    value={this.state.fields.resolverSourceBatchCollection}
                                    onChange={this.onTextInputChange}
                                />
                            </Form.Group>
                            <Form.Group
                                as={Col} sm={1}
                                controlId='resolverSourceBatchQuery'
                            >
                                <Form.Label>
                                    Query
                                </Form.Label>
                                <Form.Control
                                    type='text'
                                    value={this.state.fields.resolverSourceBatchQuery}
                                    onChange={this.onTextInputChange}
                                />
                            </Form.Group>
                            <Form.Group
                                as={Col} sm={1}
                                controlId='resolverSourceBatchDelimiter'
                            >
                                <Form.Label>
                                    Delim
                                </Form.Label>
                                <Form.Control
                                    type='text'
                                    value={this.state.fields.resolverSourceBatchDelimiter}
                                    onChange={this.onTextInputChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Group controlId='resolverResult'>
                            <Form.Label>
                                Result Field
                            </Form.Label>
                            <Form.Control
                                type='text'
                                value={this.state.fields.resolverResult}
                                onChange={this.onTextInputChange}
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <ModalFooterButtons
                    isCreate={this.state.status.create}
                    onSave={this.save}
                    onClose={this.close}
                    onRemove={this.remove}
                />
            </Modal>
        );
    }
}
