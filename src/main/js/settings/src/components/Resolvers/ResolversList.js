import React from 'react';
import PropTypes from 'prop-types';
import {Button, Table} from 'react-bootstrap';
import ResolverModal from './ResolverModal';
import Link from "../Utils/Link";

export default class ResolversList extends React.Component {
    constructor(props) {
        super(props);

        this.onUpdateClick = this.onUpdateClick.bind(this);
        this.onCreateClick = this.onCreateClick.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
    }

    onCreateClick() {
        this.modal.open(null, (newResolver) => {
            this.props.onCreate(newResolver);
        })
    }

    onUpdateClick(resolver) {
        this.modal.open(resolver, (newResolver) => {
            this.props.onUpdate(resolver.id, newResolver);
        });
    }

    onDeleteClick(resolver) {
        this.props.onDelete(resolver.id);
    }

    render() {
        return (
            <>
                <h2 id='resolvers'>
                    Resolvers
                    <Button
                        variant='outline-success'
                        className='float-right'
                        onClick={this.onCreateClick}
                    >
                        <i className='fas fa-fw fa-plus'/> Create Resolver
                    </Button>
                </h2>

                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th style={{width: '20%'}}>Resolver ID</th>
                        <th style={{width: '20%'}}>Resolve URI</th>
                        <th style={{width: '30%'}}>Source field</th>
                        <th style={{width: '30%'}}>Result field</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.resolvers && this.props.resolvers.map(resolver => (
                        <tr
                            key={resolver.id}
                            className='cursor-pointer'
                            onClick={() => this.onUpdateClick(resolver)}
                        >
                            <td>
                                {resolver.id}
                            </td>
                            <td>
                                {
                                    resolver.source.batch ?
                                        <>
                                            {'* '}
                                            <Link uri={resolver.source.batch.uri}/>
                                        </> :
                                        <Link uri={resolver.source.uri}/>
                                }
                            </td>
                            <td>
                                <code
                                    className='span-ellipsis'
                                    title={resolver.source.field}
                                >
                                    {resolver.source.field}
                                </code>
                            </td>
                            <td>
                                <code
                                    className='span-ellipsis'
                                    title={resolver.result}
                                >
                                    {resolver.result}
                                </code>
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>

                <ResolverModal ref={modal => this.modal = modal}/>
            </>
        );
    }
}

ResolversList.propTypes = {
    resolvers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        source: PropTypes.shape({
            field: PropTypes.string.isRequired,
            uri: PropTypes.string,
            batch: PropTypes.shape({
                uri: PropTypes.string,
                collection: PropTypes.string,
                query: PropTypes.string,
                delimiter: PropTypes.string
            })
        }),
        result: PropTypes.string
    })).isRequired,
    onCreate: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
};
