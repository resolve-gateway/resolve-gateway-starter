import React from 'react';
import ResolversList from './ResolversList';
import {shallow} from 'enzyme';

describe('ResolversList', () => {

    it('Should render resolver', () => {
        const resolver = {
            id: 'testId',
            source: {
                field: 'testSource',
                uri: 'testUri'
            },
            result: 'testResult'
        };
        const wrapper = shallow(
            <ResolversList
                resolvers={[resolver]}
                onCreate={() => 0}
                onUpdate={() => 0}
                onDelete={() => 0}
            />
        );
        const link =
            <a href={resolver.source.uri}
               title={resolver.source.uri}
               target='_blank'
               rel='noopener noreferrer'>
                {resolver.source.uri}
            </a>;
        expect(wrapper).toContainReact(link);
    });
});
