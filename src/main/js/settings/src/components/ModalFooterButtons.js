import React from 'react';
import PropTypes from 'prop-types';
import {Button, Modal} from "react-bootstrap";

export default function ModalFooterButtons(props) {
    return (
        <Modal.Footer>
            {!props.isCreate &&
            <Button
                variant='outline-danger'
                onClick={props.onRemove}
                style={{marginRight: 'auto'}}
            >
                Remove
            </Button>}
            <Button
                variant='outline-success'
                onClick={props.onSave}
            >
                {props.isCreate ? 'Create' : 'Save'}
            </Button>
            <Button
                variant='outline-secondary'
                onClick={props.onClose}
            >
                Close
            </Button>
        </Modal.Footer>
    )
}

ModalFooterButtons.propTypes = {
    isCreate: PropTypes.bool.isRequired,
    onSave: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired
};
