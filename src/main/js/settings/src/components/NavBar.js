import React from 'react';
import PropTypes from 'prop-types';
import {Nav, Navbar, NavDropdown} from 'react-bootstrap';

export default class NavBar extends React.Component {
    render() {
        return (
            <Navbar
                bg='light'
                variant='light'
                expand='lg'
            >
                <Navbar.Brand href={window.location.pathname}>
                    <i className='fas fa-fw fa-cogs'/> Resolve Settings
                </Navbar.Brand>
                <Nav className='mr-auto'>
                    <Nav.Link href='#routes'>
                        Routes
                    </Nav.Link>
                    <Nav.Link href='#resolvers'>
                        Resolvers
                    </Nav.Link>
                    <Nav.Link href='#postmappers'>
                        Post Mappers
                    </Nav.Link>
                    <NavDropdown
                        title='Export'
                        id='export-dropdown'
                    >
                        <NavDropdown.Item onClick={this.props.onExport}>
                            YAML Configuration
                        </NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Navbar>
        )
    }
}

NavBar.propTypes = {
    onExport: PropTypes.func.isRequired
};
