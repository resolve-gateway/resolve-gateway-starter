export const gatewayPredicates = [
    'After',
    'Before',
    'Between',
    'Cookie',
    'Header',
    'Host',
    'Method',
    'Path',
    'Query',
    'RemoteAddr'
];

export const gatewayFilters = [
    'AddRequestHeader',
    'AddRequestParameter',
    'AddResponseHeader',
    'Hystrix',
    'Path',
    'PrefixPath',
    'PreserveHostHeader',
    'RequestRateLimiter',
    'RedirectTo',
    'RemoveRequestHeader',
    'RemoveResponseHeader',
    'RewritePath',
    'RewriteResponseHeader',
    'SaveSession',
    'SetPath',
    'SetResponseHeader',
    'SetStatus',
    'StripPrefix',
    'Retry',
    'RequestSize'
];
