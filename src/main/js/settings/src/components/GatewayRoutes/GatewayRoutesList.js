import React from 'react';
import {connect} from 'react-redux';
import {Button, Table} from 'react-bootstrap';
import GatewayRouteModal from './GatewayRouteModal';
import {retrieveResolvers} from '../../store/resolvers/actions';
import {retrievePostMappers} from '../../store/postMappers/actions';
import {
    createGatewayRoute,
    deleteGatewayRouteById,
    retrieveGatewayRoutes,
    updateGatewayRoute,
} from '../../store/gatewayRoutes/actions';
import {
    createResolveRoute,
    deleteResolveRouteById,
    resetResolveRoute,
    retrieveResolveRouteById,
    retrieveResolveRoutes,
    updateResolveRoute
} from '../../store/resolveRoutes/actions';

class GatewayRoutesList extends React.Component {
    constructor(props) {
        super(props);

        this.onUpdateClick = this.onUpdateClick.bind(this);
        this.onCreateClick = this.onCreateClick.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
    }

    componentDidMount() {
        this.props.retrieveGatewayRoutes();
    }

    onCreateClick() {
        this.modal.open(
            null,
            (newGatewayRoute) => {
                this.props.createGatewayRoute(newGatewayRoute);
            },
            (newResolveRoute) => {
                if (newResolveRoute.collection !== ''
                    || newResolveRoute.resolvers.length > 0
                    || newResolveRoute.postMappers.length > 0) {
                    this.props.createResolveRoute(newResolveRoute);
                }
            },
            null
        )
    }

    onUpdateClick(gatewayRoute) {
        this.props.retrieveResolveRouteById(gatewayRoute.id);
        this.modal.open(
            gatewayRoute,
            (newGatewayRoute) => {
                this.props.updateGatewayRoute(gatewayRoute.id, newGatewayRoute);
            },
            (newResolveRoute) => {
                this.props.updateResolveRoute(gatewayRoute.id, newResolveRoute);
            },
            () => {
                this.props.deleteGatewayRouteById(gatewayRoute.id);
                this.props.deleteResolveRouteById(gatewayRoute.id)
            }
        );
    }

    onDeleteClick(gatewayRoute) {
        this.props.deleteGatewayRouteById(gatewayRoute.id);
    }

    render() {
        return (
            <>
                <h2 id='routes'>
                    Routes
                    <Button
                        variant='outline-success'
                        className='float-right'
                        onClick={this.onCreateClick}
                    >
                        <i className='fas fa-fw fa-plus'/> Create Route
                    </Button>
                </h2>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th style={{width: '20%'}}>Route ID</th>
                        <th style={{width: '20%'}}>URI</th>
                        <th style={{width: '25%'}}>Predicates</th>
                        <th style={{width: '25%'}}>Filters</th>
                        <th style={{width: '10%'}}>Order</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.gatewayRoutes && this.props.gatewayRoutes.map(route => (
                        <tr
                            key={route.id}
                            className='cursor-pointer'
                            onClick={() => this.onUpdateClick(route)}
                        >
                            <td>
                                {route.id}
                            </td>
                            <td>
                                <a href={route.uri}
                                   title={route.uri}
                                   target='_blank'
                                   rel='noopener noreferrer'>
                                    {route.uri}
                                </a>
                            </td>
                            <td>
                                {route.predicates && route.predicates.length
                                    ? route.predicates.map((predicate, index) => (
                                        <div key={route.id + '_' + index} className='ellipsis'>
                                            <span title={`${predicate.name}=${predicate.args}`}>
                                                {predicate.name}=<code>{predicate.args}</code>
                                            </span>
                                        </div>
                                    ))
                                    : '—'
                                }
                            </td>
                            <td>
                                {route.filters && route.filters.length
                                    ? route.filters.map((filter, index) => (
                                        <div key={route.id + '_' + index} className='ellipsis'>
                                            <span title={`${filter.name}=${filter.args}`}>
                                                {filter.name}=<code>{filter.args}</code>
                                            </span>
                                        </div>
                                    ))
                                    : '—'
                                }
                            </td>
                            <td>
                                {route.order}
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>

                <GatewayRouteModal
                    ref={modal => this.modal = modal}
                    resolveRoute={this.props.resolveRoute}
                    resetResolveRoute={this.props.resetResolveRoute}
                    resolvers={this.props.resolvers}
                    postMappers={this.props.postMappers}
                />
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    gatewayRoutes: state.gatewayRoutes.list,
    resolveRoute: state.resolveRoutes.item,
    resolvers: state.resolvers.list,
    postMappers: state.postMappers.list,
});

const mapDispatchToProps = ({
    retrieveResolvers,
    retrievePostMappers,

    createGatewayRoute,
    deleteGatewayRouteById,
    retrieveGatewayRoutes,
    updateGatewayRoute,

    createResolveRoute,
    deleteResolveRouteById,
    resetResolveRoute,
    retrieveResolveRouteById,
    retrieveResolveRoutes,
    updateResolveRoute
});

export default connect(mapStateToProps, mapDispatchToProps)(GatewayRoutesList);
