import React from 'react';
import PropTypes from 'prop-types';
import {Alert, Button, Col, Form, InputGroup, Modal} from 'react-bootstrap';
import ModalFooterButtons from '../ModalFooterButtons';
import * as config from './GatewayRouteConfig';

export default class GatewayRouteModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = this.initialState();

        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.save = this.save.bind(this);
        this.remove = this.remove.bind(this);

        this.addPredicate = this.addPredicate.bind(this);
        this.addFilter = this.addFilter.bind(this);
        this.removePredicate = this.removePredicate.bind(this);
        this.removeFilter = this.removeFilter.bind(this);

        this.onTextInputChange = this.onTextInputChange.bind(this);
        this.onTextSubfieldInputChange = this.onTextSubfieldInputChange.bind(this);
        this.onMultipleSelectInputChange = this.onMultipleSelectInputChange.bind(this);
        this.onSelectInputChange = this.onSelectInputChange.bind(this);
        this.onCheckboxChange = this.onCheckboxChange.bind(this);
    }

    initialState() {
        return {
            status: {
                create: false,
                show: false,
                error: null
            },
            fields: {
                routeId: '',
                routeUri: '',
                routeOrder: 0,
                routePredicates: [],
                routeFilters: [],
                routeResolvers: [],
                routePostMappers: [],
                routeCollection: '',
                routeUseCollection: false
            },
            callbacks: {
                onGatewaySave: null,
                onRemove: null,
                onResolveSave: null
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.resolveRoute) {
            (this.state.fields.routeResolvers || this.state.fields.routeResolvers.length === 0)
            && (this.state.fields.routePostMappers || this.state.fields.routePostMappers.length === 0)
            && this.setState({
                fields: {
                    ...this.state.fields,
                    routeResolvers: nextProps.resolveRoute.resolvers,
                    routePostMappers: nextProps.resolveRoute.postMappers,
                    routeCollection: nextProps.resolveRoute.collection
                }
            });
        }
    }

    open(gatewayRoute, onGatewaySave, onResolveSave, onRemove) {
        this.setState({
            status: {
                ...this.state.status,
                create: gatewayRoute === null,
                show: true
            },
            fields: {
                ...this.state.fields,
                routeId: (gatewayRoute && gatewayRoute.id) || '',
                routeUri: (gatewayRoute && gatewayRoute.uri) || '',
                routeOrder: (gatewayRoute && gatewayRoute.order) || 0,
                routePredicates: (gatewayRoute && gatewayRoute.predicates) || [{
                    name: 'Path',
                    args: ''
                }],
                routeFilters: (gatewayRoute && gatewayRoute.filters) || [],
                routeResolvers: [],
                routePostMappers: [],
            },
            callbacks: {
                onGatewaySave: onGatewaySave,
                onResolveSave: onResolveSave,
                onRemove: onRemove
            }
        });
    }

    close() {
        this.props.resetResolveRoute && this.props.resetResolveRoute();
        this.setState(this.initialState());
    }

    save() {
        let order = this.state.fields.routeOrder;
        try {
            order = parseInt(order);
        } catch (e) {
            order = 0;
        }

        this.state.callbacks.onGatewaySave && this.state.callbacks.onGatewaySave({
            id: this.state.fields.routeId,
            uri: this.state.fields.routeUri,
            order: order,
            predicates: this.state.fields.routePredicates,
            filters: this.state.fields.routeFilters
        });

        this.state.callbacks.onResolveSave && this.state.callbacks.onResolveSave({
            id: this.state.fields.routeId,
            useCollection: this.state.fields.routeUseCollection,
            collection: this.state.fields.routeCollection,
            resolvers: this.state.fields.routeResolvers,
            postMappers: this.state.fields.routePostMappers,
        });

        this.close();
    }

    remove() {
        this.state.callbacks.onRemove && this.state.callbacks.onRemove();
        this.close();
    }

    addPredicate() {
        this.setState({
            fields: {
                ...this.state.fields,
                routePredicates: this.state.fields.routePredicates.concat({
                    name: config.gatewayPredicates[0],
                    args: ''
                })
            }
        })
    }

    removePredicate(index) {
        this.setState({
            fields: {
                ...this.state.fields,
                routePredicates: this.state.fields.routePredicates.filter((_, i) => i !== index)
            }
        });
    }

    addFilter() {
        this.setState({
            fields: {
                ...this.state.fields,
                routeFilters: this.state.fields.routeFilters.concat({
                    name: config.gatewayFilters[0],
                    args: ''
                })
            }
        })
    }

    removeFilter(index) {
        this.setState({
            fields: {
                ...this.state.fields,
                routeFilters: this.state.fields.routeFilters.filter((_, i) => i !== index)
            }
        });
    }

    onTextInputChange(e) {
        const id = e.target.id;
        const value = e.target.value;

        this.setState({
            fields: {
                ...this.state.fields,
                [id]: value
            }
        });
    }

    onTextSubfieldInputChange(e) {
        const id = e.target.id;
        const [field, subfield, indexStr] = id.split('_');
        const index = parseInt(indexStr);
        const value = e.target.value;

        this.setState({
            fields: {
                ...this.state.fields,
                [field]: this.state.fields[field].map((p, i) => {
                    if (i !== index) {
                        return p;
                    }
                    return {
                        ...p,
                        [subfield]: value
                    }
                })
            }
        });
    }

    onMultipleSelectInputChange(e) {
        const id = e.target.id;
        const options = [...e.target.options]
            .filter(o => o.selected)
            .map(o => o.value);

        this.setState({
            fields: {
                ...this.state.fields,
                [id]: options
            }
        });
    }

    onSelectInputChange(e) {
        const id = e.target.id;
        const [field, subfield, indexStr] = id.split('_');
        const index = parseInt(indexStr);
        const selected = [...e.target.options]
            .filter(o => o.selected)
            .map(o => o.value)[0];

        this.setState({
            fields: {
                ...this.state.fields,
                [field]: this.state.fields[field].map((p, i) => {
                    if (i !== index) {
                        return p;
                    }
                    return {
                        ...p,
                        [subfield]: selected
                    }
                })
            }
        })
    }

    onCheckboxChange(e) {
        const id = e.target.id;
        const checked = e.target.checked;

        this.setState({
            fields: {
                ...this.state.fields,
                [id]: checked
            }
        })
    }

    render() {
        return (
            <Modal
                size='lg'
                show={this.state.status.show}
                onHide={this.close}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        {this.state.status.create ? 'Create ' : 'Update '}
                        Route
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.state.status.error &&
                    <Alert variant='danger'>
                        <Alert.Heading>Error</Alert.Heading>
                        <p>{this.state.status.error}</p>
                    </Alert>}
                    <Form autoComplete='off'>
                        <Form.Row>
                            <Form.Group
                                controlId='routeId'
                                as={Col} sm={10}
                            >
                                <Form.Label>
                                    Route ID
                                </Form.Label>
                                <Form.Control
                                    type='text'
                                    placeholder="E.g. ' documents'"
                                    value={this.state.fields.routeId}
                                    onChange={this.onTextInputChange}
                                />
                            </Form.Group>
                            <Form.Group
                                controlId='routeOrder'
                                as={Col} sm={2}
                            >
                                <Form.Label>
                                    Order
                                </Form.Label>
                                <Form.Control
                                    type='text'
                                    placeholder='E.g. 0'
                                    value={this.state.fields.routeOrder}
                                    onChange={this.onTextInputChange}
                                />
                            </Form.Group>
                        </Form.Row>
                        <Form.Group controlId='routeUri'>
                            <Form.Label>
                                URI
                            </Form.Label>
                            <InputGroup>
                                <Form.Control
                                    type='text'
                                    placeholder="E.g. ' http://proxied-service:8080'"
                                    value={this.state.fields.routeUri}
                                    onChange={this.onTextInputChange}
                                />
                                <InputGroup.Append>
                                    <Button
                                        variant='outline-primary'
                                        size='sm'
                                        onClick={() => this.state.fields.routeUri !== '' && window.open(this.state.fields.routeUri, "_blank")}
                                    >
                                        <i className='fas fa-sm fa-fw fa-external-link-alt'/>
                                    </Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                        <hr/>
                        <div className='clearfix'>
                            <strong>
                                <a href="https://cloud.spring.io/spring-cloud-gateway/single/spring-cloud-gateway.html#gateway-request-predicates-factories"
                                   rel='noopener noreferrer'
                                   target='_blank'
                                >
                                    Predicates
                                </a>
                            </strong>
                            <Button
                                variant='outline-success'
                                size='sm'
                                className='float-right'
                                onClick={this.addPredicate}
                            >
                                <i className='fas fa-sm fa-fw fa-plus'/>
                            </Button>
                        </div>
                        {this.state.fields.routePredicates.map((predicate, i) => (
                            <Form.Row key={'routePredicates_' + i}>
                                <Form.Group
                                    controlId={'routePredicates_name_' + i}
                                    as={Col} sm={3}
                                >
                                    {i === 0 &&
                                    <Form.Label>
                                        Name
                                    </Form.Label>}
                                    <Form.Control
                                        as='select'
                                        size='sm'
                                        value={predicate.name}
                                        onChange={this.onSelectInputChange}
                                    >
                                        {config.gatewayPredicates.map(p => (
                                            <option key={p} value={p}>
                                                {p}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group
                                    controlId={'routePredicates_args_' + i}
                                    as={Col} sm={9}
                                >
                                    {i === 0 &&
                                    <Form.Label>
                                        Arguments
                                    </Form.Label>}
                                    <InputGroup>
                                        <Form.Control
                                            type='text'
                                            size='sm'
                                            placeholder="E.g. ' args'"
                                            value={predicate.args}
                                            onChange={this.onTextSubfieldInputChange}
                                        />
                                        <InputGroup.Append>
                                            <Button
                                                variant='outline-danger'
                                                size='sm'
                                                onClick={() => this.removePredicate(i)}
                                            >
                                                <i className='fas fa-sm fa-fw fa-minus'/>
                                            </Button>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Form.Group>
                            </Form.Row>
                        ))}
                        <hr/>
                        <div className='clearfix'>
                            <strong>
                                <a
                                    href="https://cloud.spring.io/spring-cloud-gateway/single/spring-cloud-gateway.html#_gatewayfilter_factories"
                                    rel='noopener noreferrer'
                                    target='_blank'
                                >
                                    Filters
                                </a>
                            </strong>
                            <Button
                                variant='outline-success'
                                size='sm'
                                className='float-right'
                                onClick={this.addFilter}
                            >
                                <i className='fas fa-sm fa-fw fa-plus'/>
                            </Button>
                        </div>
                        {this.state.fields.routeFilters.map((filter, i) => (
                            <Form.Row key={'routeFilters_' + i}>
                                <Form.Group
                                    controlId={'routeFilters_name_' + i}
                                    as={Col} sm={3}
                                >
                                    {i === 0 &&
                                    <Form.Label>
                                        Name
                                    </Form.Label>}
                                    <Form.Control
                                        as='select'
                                        size='sm'
                                        value={filter.name}
                                        onChange={this.onSelectInputChange}
                                    >
                                        {config.gatewayFilters.map(f => (
                                            <option key={f} value={f}>
                                                {f}
                                            </option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                                <Form.Group
                                    controlId={'routeFilters_args_' + i}
                                    as={Col} sm={9}
                                >
                                    {i === 0 &&
                                    <Form.Label>
                                        Arguments
                                    </Form.Label>}
                                    <InputGroup>
                                        <Form.Control
                                            type='text'
                                            size='sm'
                                            placeholder="E.g. ' args'"
                                            value={filter.args}
                                            onChange={this.onTextSubfieldInputChange}
                                        />
                                        <InputGroup.Append>
                                            <Button
                                                variant='outline-danger'
                                                size='sm'
                                                onClick={() => this.removeFilter(i)}
                                            >
                                                <i className='fas fa-sm fa-fw fa-minus'/>
                                            </Button>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Form.Group>
                            </Form.Row>
                        ))}
                        <hr/>
                        <Form.Group controlId='routeCollection'>
                            <Form.Label>
                                Resolve collection name
                            </Form.Label>
                            <InputGroup>
                                <InputGroup.Prepend>
                                    <InputGroup.Checkbox
                                        id='routeUseCollection'
                                        title="Use collection path to resolve"
                                        checked={this.state.fields.routeUseCollection}
                                        onChange={this.onCheckboxChange}/>
                                </InputGroup.Prepend>
                                <Form.Control
                                    type='text'
                                    placeholder="E.g. _embedded.documents"
                                    value={this.state.fields.routeCollection}
                                    onChange={this.onTextInputChange}
                                />
                            </InputGroup>
                        </Form.Group>
                        <Form.Row>
                            <Form.Group
                                controlId='routeResolvers'
                                as={Col} sm={6}
                            >
                                <Form.Label>
                                    <strong>
                                        Resolvers
                                    </strong>
                                </Form.Label>
                                <Form.Control
                                    as='select' multiple
                                    value={this.state.fields.routeResolvers}
                                    onChange={this.onMultipleSelectInputChange}
                                >
                                    {this.props.resolvers && this.props.resolvers.map(resolver => (
                                        <option key={resolver.id} value={resolver.id}>
                                            {resolver.id}
                                        </option>
                                    ))}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group
                                controlId='routePostMappers'
                                as={Col} sm={6}
                            >
                                <Form.Label>
                                    <strong>
                                        Post Mappers
                                    </strong>
                                </Form.Label>
                                <Form.Control
                                    as='select' multiple
                                    value={this.state.fields.routePostMappers}
                                    onChange={this.onMultipleSelectInputChange}
                                >
                                    {this.props.postMappers && this.props.postMappers.map(postMapper => (
                                        <option key={postMapper.id} value={postMapper.id}>
                                            {postMapper.id}
                                        </option>
                                    ))}
                                </Form.Control>
                            </Form.Group>
                        </Form.Row>
                    </Form>
                </Modal.Body>
                <ModalFooterButtons
                    isCreate={this.state.status.create}
                    onSave={this.save}
                    onClose={this.close}
                    onRemove={this.remove}
                />
            </Modal>
        );
    }
}

GatewayRouteModal.propTypes = {
    resolveRoute: PropTypes.shape({
        id: PropTypes.string.isRequired,
        collection: PropTypes.string,
        resolvers: PropTypes.array,
        postMappers: PropTypes.array
    }),
    resetResolveRoute: PropTypes.func.isRequired,
    resolvers: PropTypes.array.isRequired,
    postMappers: PropTypes.array.isRequired,
};