import React from 'react';
import NavBar from './NavBar';
import {shallow} from 'enzyme';

describe('Navigation bar', () => {

    it('Should render menu', () => {
        const wrapper = shallow(
            <NavBar
                onExport={() => 0}
            />
        );
        expect(wrapper.text().includes('Routes')).toBe(true);
        expect(wrapper.text().includes('Resolvers')).toBe(true);
        expect(wrapper.text().includes('Post Mappers')).toBe(true);
    });
});
