import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import getStore from './store/store';
import App from './components/App';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();
const store = getStore(history);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
