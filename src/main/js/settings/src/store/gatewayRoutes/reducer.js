import * as t from './types';

const initialState = {
    list: []
};

const sortByOrder = (a, b) => a.order - b.order;

const distinctById = (v, i, a) => a.map(a => a.id).indexOf(v.id) === i; // imitate unique key because of spring impl

export const gatewayRoutesReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.ACTION_GATEWAY_ROUTES_RETRIEVE: {
            return {
                ...state,
                list: action.payload
                    .filter(distinctById)
            }
        }
        case t.ACTION_GATEWAY_ROUTES_CREATE: {
            return {
                ...state,
                list: [...state.list, action.payload]
                    .sort(sortByOrder)
                    .filter(distinctById)
            }
        }
        case t.ACTION_GATEWAY_ROUTES_UPDATE: {
            return {
                ...state,
                list: state.list.map(item => item.id !== action.payload.id
                    ? item
                    : action.payload.gatewayRoute)
                    .sort(sortByOrder)
                    .filter(distinctById)
            }
        }
        case t.ACTION_GATEWAY_ROUTES_DELETE_BY_ID:
        case t.ACTION_GATEWAY_ROUTES_DELETE_BY_ID_FAILED: { // imitate deletion because routes from config are immutable...
            return {
                ...state,
                list: state.list.filter(item => item.id !== action.payload)
            }
        }
        default: {
            return {
                ...state
            }
        }
    }
};

export default gatewayRoutesReducer;

