import {createById, deleteById, retrieveAll, updateById} from '../../services/gatewayRoutes';
import * as t from './types';

export const createGatewayRoute = (gatewayRoute) => async dispatch => {
    try {
        gatewayRoute = await createById(gatewayRoute);
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_CREATE,
            payload: gatewayRoute,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_CREATE_FAILED,
            payload: err,
        });
    }
};

export const retrieveGatewayRoutes = () => async dispatch => {
    try {
        const gatewayRoutes = await retrieveAll();
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_RETRIEVE,
            payload: gatewayRoutes,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_RETRIEVE_FAILED,
            payload: err,
        });
    }
};

export const updateGatewayRoute = (id, gatewayRoute) => async dispatch => {
    try {
        await updateById(id, gatewayRoute);
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_UPDATE,
            payload: {
                id: id,
                gatewayRoute: gatewayRoute
            },
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_UPDATE_FAILED,
            payload: err,
        });
    }
};

export const deleteGatewayRouteById = (id) => async dispatch => {
    try {
        await deleteById(id);
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_DELETE_BY_ID,
            payload: id,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_GATEWAY_ROUTES_DELETE_BY_ID_FAILED,
            payload: id,
        });
    }
};
