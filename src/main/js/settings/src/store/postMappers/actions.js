import {create, deleteById, retrieveAll, updateById} from '../../services/postMappers';
import * as t from './types';

export const createPostMapper = (postMapper) => async dispatch => {
    try {
        postMapper = await create(postMapper);
        dispatch({
            type: t.ACTION_POST_MAPPERS_CREATE,
            payload: postMapper,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_POST_MAPPERS_CREATE_FAILED,
            payload: err,
        });
    }
};

export const retrievePostMappers = () => async dispatch => {
    try {
        const postMappers = await retrieveAll();
        dispatch({
            type: t.ACTION_POST_MAPPERS_RETRIEVE,
            payload: postMappers,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_POST_MAPPERS_RETRIEVE_FAILED,
            payload: err,
        });
    }
};

export const updatePostMapper = (id, postMapper) => async dispatch => {
    try {
        await updateById(id, postMapper);
        dispatch({
            type: t.ACTION_POST_MAPPERS_UPDATE,
            payload: {
                id: id,
                postMapper: postMapper
            },
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_POST_MAPPERS_UPDATE_FAILED,
            payload: err,
        });
    }
};

export const deletePostMapperById = (id) => async dispatch => {
    try {
        await deleteById(id);
        dispatch({
            type: t.ACTION_POST_MAPPERS_DELETE_BY_ID,
            payload: id,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_POST_MAPPERS_DELETE_BY_ID_FAILED,
            payload: err,
        });
    }
};
