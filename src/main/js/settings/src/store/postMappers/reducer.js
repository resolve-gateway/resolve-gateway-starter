import * as t from './types';

const initialState = {
    list: []
};

export const postMappersReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.ACTION_POST_MAPPERS_RETRIEVE: {
            return {
                ...state,
                list: action.payload,
            }
        }
        case t.ACTION_POST_MAPPERS_CREATE: {
            return {
                ...state,
                list: [...state.list, action.payload]
            }
        }
        case t.ACTION_POST_MAPPERS_UPDATE: {
            return {
                ...state,
                list: state.list.map(item => item.id !== action.payload.id
                    ? item
                    : action.payload.postMapper)
            }
        }
        case t.ACTION_POST_MAPPERS_DELETE_BY_ID: {
            return {
                ...state,
                list: state.list.filter(item => item.id !== action.payload)
            }
        }
        default: {
            return {
                ...state
            }
        }
    }
};

export default postMappersReducer;

