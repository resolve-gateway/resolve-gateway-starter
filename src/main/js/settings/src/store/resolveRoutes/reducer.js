import * as t from './types';

const initialState = {
    list: [],
    item: null
};

const sortByOrder = (a, b) => a.order - b.order;

export const resolveRoutesReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.ACTION_RESOLVE_ROUTES_RETRIEVE: {
            return {
                ...state,
                list: action.payload,
            }
        }
        case t.ACTION_RESOLVE_ROUTES_CREATE: {
            return {
                ...state,
                list: [...state.list, action.payload]
                    .sort(sortByOrder)
            }
        }
        case t.ACTION_RESOLVE_ROUTES_UPDATE: {
            return {
                ...state,
                list: state.list.map(item => item.id !== action.payload.id
                    ? item
                    : action.payload.gatewayRoute)
                    .sort(sortByOrder)
            }
        }
        case t.ACTION_RESOLVE_ROUTES_DELETE_BY_ID: {
            return {
                ...state,
                list: state.list.filter(item => item.id !== action.payload)
            }
        }
        case t.ACTION_RESOLVE_ROUTES_RETRIEVE_BY_ID: {
            return {
                ...state,
                item: action.payload
            }
        }
        case t.ACTION_RESOLVE_ROUTES_RESET: {
            return {
                ...state,
                item: null
            }
        }
        default: {
            return {
                ...state
            }
        }
    }
};

export default resolveRoutesReducer;

