import {createById, deleteById, retrieveAll, retrieveById, updateById} from '../../services/resolveRoutes';
import * as t from './types';

export const createResolveRoute = (resolveRoute) => async dispatch => {
    try {
        resolveRoute = await createById(resolveRoute);
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_CREATE,
            payload: resolveRoute,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_CREATE_FAILED,
            payload: err,
        });
    }
};

export const retrieveResolveRoutes = () => async dispatch => {
    try {
        const resolveRoutes = await retrieveAll();
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_RETRIEVE,
            payload: resolveRoutes,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_RETRIEVE_FAILED,
            payload: err,
        });
    }
};

export const updateResolveRoute = (id, resolveRoute) => async dispatch => {
    try {
        await updateById(id, resolveRoute);
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_UPDATE,
            payload: {
                id: id,
                resolveRoute: resolveRoute
            },
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_UPDATE_FAILED,
            payload: err,
        });
    }
};

export const deleteResolveRouteById = (id) => async dispatch => {
    try {
        await deleteById(id);
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_DELETE_BY_ID,
            payload: id,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_DELETE_BY_ID_FAILED,
            payload: id,
        });
    }
};

export const retrieveResolveRouteById = (id) => async dispatch => {
    try {
        const resolveRoute = await retrieveById(id);
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_RETRIEVE_BY_ID,
            payload: resolveRoute,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_RETRIEVE_BY_ID_FAILED,
            payload: err,
        });
    }
};

export const resetResolveRoute = () => async dispatch => {
    try {
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_RESET,
            payload: {},
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVE_ROUTES_RESET_FAILED,
            payload: err,
        });
    }
};
