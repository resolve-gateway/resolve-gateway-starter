import resolversReducer from './resolvers/reducer';
import postMappersReducer from './postMappers/reducer'
import gatewayRoutesReducer from './gatewayRoutes/reducer';
import resolveRouteReducer from './resolveRoutes/reducer';

export default {
    resolvers: resolversReducer,
    postMappers: postMappersReducer,
    gatewayRoutes: gatewayRoutesReducer,
    resolveRoutes: resolveRouteReducer
};
