import * as t from './types';

const initialState = {
    list: [],
    error: null
};

export const resolverReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.ACTION_RESOLVERS_RETRIEVE: {
            return {
                ...state,
                list: action.payload,
            }
        }
        case t.ACTION_RESOLVERS_CREATE: {
            return {
                ...state,
                list: [...state.list, action.payload]
            }
        }
        case t.ACTION_RESOLVERS_UPDATE: {
            return {
                ...state,
                list: state.list.map(item => item.id !== action.payload.id
                    ? item
                    : action.payload.resolver)
            }
        }
        case t.ACTION_RESOLVERS_DELETE_BY_ID: {
            return {
                ...state,
                list: state.list.filter(item => item.id !== action.payload.id)
            }
        }
        default: {
            return {
                ...state,
                error: action.payload
            }
        }
    }
};

export default resolverReducer;

