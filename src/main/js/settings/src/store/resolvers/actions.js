import {create, deleteById, retrieveAll, updateById} from '../../services/resolvers';
import * as t from './types';

export const createResolver = (resolver) => async dispatch => {
    try {
        resolver = await create(resolver);
        dispatch({
            type: t.ACTION_RESOLVERS_CREATE,
            payload: resolver,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVERS_CREATE_FAILED,
            payload: err,
        });
    }
};

export const retrieveResolvers = () => async dispatch => {
    try {
        const resolvers = await retrieveAll();
        dispatch({
            type: t.ACTION_RESOLVERS_RETRIEVE,
            payload: resolvers,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVERS_RETRIEVE_FAILED,
            payload: err,
        });
    }
};

export const updateResolver = (id, resolver) => async dispatch => {
    try {
        await updateById(id, resolver);
        dispatch({
            type: t.ACTION_RESOLVERS_UPDATE,
            payload: {
                id: id,
                resolver: resolver
            },
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVERS_UPDATE_FAILED,
            payload: err,
        });
    }
};

export const deleteResolver = (resolver) => async dispatch => {
    try {
        await deleteById(resolver.id);
        dispatch({
            type: t.ACTION_RESOLVERS_DELETE_BY_ID,
            payload: resolver,
        });
    } catch (err) {
        dispatch({
            type: t.ACTION_RESOLVERS_DELETE_BY_ID_FAILED,
            payload: err,
        });
    }
};
