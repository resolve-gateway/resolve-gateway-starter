import axios from 'axios/index';
import config from '../config'

const api = config.api.postMappers;

export const create = async (item) => {
    try {
        const resp = await axios({
            url: api,
            method: 'post',
            data: item,
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const retrieveAll = async () => {
    try {
        const resp = await axios({
            url: api,
            method: 'get',
            headers: {},
        });

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const updateById = async (id, item) => {
    try {
        const resp = await axios({
            url: `${api}/${id}`,
            method: 'post',
            data: item,
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const deleteById = async (id) => {
    try {
        const resp = await axios({
            url: `${api}/${id}`,
            method: 'delete',
            headers: {}
        });

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};
