import axios from 'axios/index';
import config from '../config'

const gatewayRoutes = config.api.gatewayRoutes;
const gatewayRefresh = config.api.gatewayRefresh;

const itemToRoute = (item) => ({
    ...item,
    predicates: item.predicates.map(predicate => ({
        name: predicate.name,
        args: predicate.args.split(',')
            .map(it => it.trim())
            .reduce((map, value, i) => {
                map['_genkey_' + i] = value;
                return map;
            }, {})
    })),
    filters: item.filters.map(filter => ({
        name: filter.name,
        args: filter.args.split(',')
            .map(it => it.trim())
            .reduce((map, value, i) => {
                map['_genkey_' + i] = value;
                return map;
            }, {})
    }))
});

const refreshGatewayRoutes = () => {
    axios({
        url: gatewayRefresh,
        method: 'post',
        headers: {}
    });
};

export const createById = async (item) => {
    try {
        const route = itemToRoute(item);

        await axios({
            url: `${gatewayRoutes}/${route.id}`,
            method: 'post',
            data: route,
            headers: {}
        });

        refreshGatewayRoutes();

        return item;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const retrieveAll = async () => {
    try {
        const resp = await axios({
            url: gatewayRoutes,
            method: 'get',
            headers: {},
        });

        return resp.data
            .map(route => route.route_definition)
            .map(route => {
                route.predicates = route.predicates.map(predicate => ({
                    name: predicate.name,
                    args: Object.values(predicate.args).join(', ')
                }));
                route.filters = route.filters.map(filter => ({
                    name: filter.name,
                    args: Object.values(filter.args).join(', ')
                }));
                return route;
            });
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const updateById = async (id, item) => {
    try {
        const route = itemToRoute(item);

        if (route.id !== id) {
            await deleteById(id);
        }

        await axios({
            url: `${gatewayRoutes}/${route.id}`,
            method: 'post',
            data: route,
            headers: {}
        });

        refreshGatewayRoutes();

        return item;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const deleteById = async (id) => {
    try {
        const resp = await axios({
            url: `${gatewayRoutes}/${id}`,
            method: 'delete',
            headers: {}
        });

        refreshGatewayRoutes();

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};
