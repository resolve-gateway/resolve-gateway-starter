import axios from 'axios/index';
import config from '../config'

const resolveApi = config.api.resolveRoutes;

export const createById = async (item) => {
    try {
        await axios({
            url: `${resolveApi}/${item.id}`,
            method: 'post',
            data: item,
            headers: {}
        });

        return item;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const retrieveAll = async () => {
    try {
        const resp = await axios({
            url: resolveApi,
            method: 'get',
            headers: {},
        });

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const retrieveById = async (id) => {
    try {
        const resp = await axios({
            url: `${resolveApi}/${id}`,
            method: 'get',
            headers: {},
        });

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const updateById = async (id, item) => {
    try {
        if (item.id !== id) {
            await deleteById(id);
        }

        await axios({
            url: `${resolveApi}/${item.id}`,
            method: 'post',
            data: item,
            headers: {}
        });

        return item;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

export const deleteById = async (id) => {
    try {
        const resp = await axios({
            url: `${resolveApi}/${id}`,
            method: 'delete',
            headers: {}
        });

        return resp.data;
    } catch (err) {
        console.log(err);
        throw err;
    }
};
